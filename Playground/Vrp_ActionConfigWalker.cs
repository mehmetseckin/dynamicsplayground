﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Playground
{
    internal class Vrp_ActionConfigWalker
    {
        private static List<string> fieldsFilled = new List<string>();
        private static List<string> decisionsMade = new List<string>();
        
        private static StringBuilder log = new StringBuilder();
        public static StringBuilder Log
        {
            get { return Vrp_ActionConfigWalker.log; }
        }

        private static List<string> path = new List<string>();
        public static List<string> Path
        {
            get 
            {
                path.Sort(new Helpers.ActionCodeComparer());
                return Vrp_ActionConfigWalker.path; 
            }
            set { Vrp_ActionConfigWalker.path = value; }
        }

        public Vrp_ActionConfigWalker(vrp_investdocument document)
        {
            this.Config = Helpers.GetActionConfig("1.1");
            this.Document = document;        }

        public Vrp_ActionConfigWalker(vrp_investdocument document, vrp_actionconfig config)
        {
            this.Config = config;
            this.Document = document;
        }

        public vrp_actionconfig Config { get; set; }

        public string ActionInfo { get { return Config.vrp_actioncode + " : " + Config.vrp_description; } }

        public string ActionCode { get { return Config.vrp_actioncode; } }

        public vrp_investdocument Document { get; set; }

        public bool IsDecision
        {
            get
            {
                return ((Config.Attributes.ContainsKey("vrp_decision") && !String.IsNullOrEmpty(Config.vrp_decision)) &&
                        (Config.Attributes.ContainsKey("vrp_decisionvalue") && !String.IsNullOrEmpty(Config.vrp_decisionvalue)));
            }
        }

        public string[] DecisionFields
        {
            get
            {
                if (IsDecision)
                    return Config.vrp_decision.Split('*');
                return null;
            }
        }

        public string[] ExpectedDecisionFieldValues
        {
            get
            {
                if (IsDecision)
                    return Config.vrp_decisionvalue.Split('*');
                return null;
            }
        }

        public bool DecisionValuesMatch
        {
            get
            {
                if (!IsDecision || HasConditionalSplit) return false;
                bool flag = false;
                for (int i = 0; i < DecisionFields.Length; i++)
                {
                    try { flag = ((OptionSetValue)Document[DecisionFields[i]]).Value.ToString().Equals(ExpectedDecisionFieldValues[i]); }
                    catch { flag = Document[DecisionFields[i]].ToString().Equals(ExpectedDecisionFieldValues[i]); }

                    if (IsGroupByAND)
                    {
                        if (!flag) { return false; }
                    }
                    else
                    {
                        if (flag) return true;
                    }
                }
                return false;
            }
        }

        public bool HasConditionalSplit
        {
            get
            {
                return (this.IsDecision &&
                       (Config.Attributes.ContainsKey("vrp_conditionalsplit") && Config.vrp_conditionalsplit != null && Config.vrp_conditionalsplit.Equals(true)));
            }
        }

        public int ConditionalSplitIndex
        {
            get
            {
                if (HasConditionalSplit && Document.Attributes.ContainsKey(Config.vrp_decision) && Document[Config.vrp_decision] != null)
                {
                    return Array.IndexOf(Config.vrp_decisionvalue.Split('*'), ((OptionSetValue) Document[Config.vrp_decision]).Value.ToString());
                }
                return -1;
            }
        }

        public bool StartsParallelProcess
        {
            get
            {
                if (IsDecision)
                {
                    if (HasConditionalSplit) return false;
                    if (DecisionValuesMatch)
                        return (Config.Attributes.ContainsKey("vrp_nextstepcodeifitcomestrue") && !String.IsNullOrEmpty(Config.vrp_nextstepcodeifitcomestrue) && Config.vrp_nextstepcodeifitcomestrue.Contains('*'));
                    else
                        return (Config.Attributes.ContainsKey("vrp_nextstepcodeifitdoesntcometrue") && !String.IsNullOrEmpty(Config.vrp_nextstepcodeifitdoesntcometrue) && Config.vrp_nextstepcodeifitdoesntcometrue.Contains('*'));
                }
                else
                {
                    return ((IsCompleted) ? Config.vrp_nextstepcodeifitcomestrue.Contains('*') : Config.vrp_nextstepcodeifitdoesntcometrue.Contains('*'));
                }
            }
        }

        public string[] ParallelFields
        {
            get
            {
                if (HasParallelActions)
                    return Config.vrp_paralelfield.Split('*');
                return new string[] { };
            }
        }

        public bool HasParallelActions
        {
            get
            {
                return ((Config.Attributes.ContainsKey("vrp_paralelfield") && !String.IsNullOrEmpty(Config.vrp_paralelfield)) &&
                        (Config.Attributes.ContainsKey("vrp_paralelfieldvalue") && !String.IsNullOrEmpty(Config.vrp_paralelfieldvalue)));
            }
        }

        public string[] ExpectedParallelFieldValues
        {
            get
            {
                if (HasParallelActions)
                    return Config.vrp_paralelfieldvalue.Split('*');
                return new string[] { };
            }
        }

        public bool IsGroupByOR
        {
            get
            {
                return (HasParallelActions && Config.Attributes.ContainsKey("vrp_grouping") && Config.vrp_grouping != null && Config.vrp_grouping.Value.Equals(2));
            }
        }

        public bool IsGroupByAND
        {
            get
            {
                return (HasParallelActions && Config.Attributes.ContainsKey("vrp_grouping") && Config.vrp_grouping != null && Config.vrp_grouping.Value.Equals(1));
            }
        }

        public bool IsCompleted
        {
            get
            {
                if (HasParallelActions)
                {
                    if (IsGroupByOR)
                    {
                        for (int i = 0; i < ParallelFields.Length; i++)
                            if (Document.Attributes.ContainsKey(ParallelFields[i]) &&
                                ((OptionSetValue)Document[ParallelFields[i]]).Value.ToString().Equals(ExpectedParallelFieldValues[i]))
                                return true;
                        return false;
                    }
                    if (IsGroupByAND)
                    {
                        for (int i = 0; i < ParallelFields.Length; i++)
                            if (!Document.Attributes.ContainsKey(ParallelFields[i]) ||
                                !((OptionSetValue)Document[ParallelFields[i]]).Value.ToString().Equals(ExpectedParallelFieldValues[i]))
                                return false;
                        return true;
                    }
                }
                else
                {
                    if (Config.Attributes.ContainsKey("vrp_fieldinfo") && !String.IsNullOrEmpty(Config.vrp_fieldinfo))
                        return (Document.Attributes.ContainsKey(Config.vrp_fieldinfo) && ((OptionSetValue)Document[Config.vrp_fieldinfo]).Value.ToString().Equals(Config.vrp_fieldvalueinfo));
                }
                return false;
            }
        }

        public bool DidItComeTrue
        {
            get
            {
                if (HasConditionalSplit)
                {
                    return ConditionalSplitIndex >= 0;
                }
                else if (IsDecision)
                    return DecisionValuesMatch;
                else
                {
                    if (Config.Attributes.ContainsKey("vrp_fieldinfo") && !String.IsNullOrEmpty(Config.vrp_fieldinfo))
                        return (Document.Attributes.ContainsKey(Config.vrp_fieldinfo) && ((OptionSetValue)Document[Config.vrp_fieldinfo]).Value.ToString().Equals(Config.vrp_fieldvalueinfo));
                }
                return false;
            }
        }

        public string[] NextSteps
        {
            get
            {
                try
                {
                    if (IsCompleted)
                        if (DidItComeTrue)
                            return HasConditionalSplit ? Config.vrp_nextstepcodeifitcomestrue.Split('*')[ConditionalSplitIndex].Split('*') : Config.vrp_nextstepcodeifitcomestrue.Split('*'); // WTF
                        else
                            return Config.vrp_nextstepcodeifitdoesntcometrue.Split('*');
                    return new string[] { };
                }
                catch (Exception)
                {
                    return new string[] { };                    
                }
            }
        }

        private void Prompt()
        {
            if (IsDecision)
                PromptDecision();
            PromptField();
            if (HasParallelActions)
                PromptParallel();
        }

        private void PromptField()
        {
            if (!String.IsNullOrEmpty(Config.vrp_fieldinfo))
            {
                if (fieldsFilled.Contains(Config.vrp_fieldinfo))
                    return;
                Console.WriteLine("Action Code : {0}, Status {1}", ActionInfo, "Pending");
                OptionMetadataCollection options = Helpers.GetOptions(Document.LogicalName, Config.vrp_fieldinfo);
                Console.WriteLine("Please select a value for {0} : ", Config.vrp_fieldinfo);
                for (int i = 0; i < options.Count; i++)
                {
                    OptionMetadata option = options[i];
                    string label = option.Label.LocalizedLabels[0].Label;
                    Console.WriteLine("{0}. {1} ({2})", i + 1, label, option.Value);
                }
                int decision = Helpers.ReadSelection();
                Console.WriteLine();
                decision--; // 0 based index vs 1 based selection
                if (decision > -1 && decision < options.Count)
                {
                    Helpers.SetOptionValue(Document, Config.vrp_fieldinfo, new OptionSetValue((int)options[decision].Value));
                    fieldsFilled.Add(Config.vrp_fieldinfo);
                }
            }
        }

        private void PromptParallel()
        {
            Console.WriteLine();
            Console.WriteLine("Action Code : {0}, Status {1}", ActionInfo, "Parallel");
            vrp_actionconfig originalConfig = this.Config;
            String[] parallelActions = String.Join("*", ParallelFields).Replace("vrp_", "").Replace('_', '.').Split('*');
            foreach (string actionCode in parallelActions)
            {
                this.Config = Helpers.GetActionConfig(actionCode);
                PromptField();
                if (IsDecision)
                    PromptDecision();
            }
            this.Config = originalConfig;
        }

        private void PromptDecision()
        {
            Console.WriteLine();
            Console.WriteLine("Action Code : {0}, Status {1}", ActionInfo, "Decision");

            foreach (string field in DecisionFields)
            {
                if (decisionsMade.Contains(field)) 
                    continue;
                OptionMetadataCollection options = Helpers.GetOptions(Document.LogicalName, field);
                Console.WriteLine("Please select a value for {0} : ", field);
                for (int i = 0; i < options.Count; i++)
                {
                    OptionMetadata option = options[i];
                    string label = option.Label.LocalizedLabels[0].Label;
                    Console.WriteLine("{0}. {1} ({2})", i + 1, label, option.Value);
                }
                int decision = Helpers.ReadSelection();
                decision--;
                if (decision > -1 && decision < options.Count)
                {
                    Helpers.SetOptionValue(Document, field, new OptionSetValue((int)options[decision].Value));
                    decisionsMade.Add(field);
                } 
            }

        }

        public void PromptAndWalk()
        {
            Console.Clear();
            Console.WriteLine("{0} @ {1}", Document.vrp_name, ActionInfo);

            Prompt();

            if (NextSteps.Length > 0)
            {
                foreach (string step in NextSteps)
                {
                    Vrp_ActionConfigWalker walker = new Vrp_ActionConfigWalker(Document);
                    walker.Config = Helpers.GetActionConfig(step);
                    walker.PromptAndWalk();
                }
            }
        }

        public void Walk()
        {
            Log.AppendLine(this.ToString());
            Path.Add(this.ActionCode);
            if (NextSteps.Length > 0)
            {
                foreach (string step in NextSteps)
                {
                    Vrp_ActionConfigWalker walker = new Vrp_ActionConfigWalker(Document);
                    walker.Config = Helpers.GetActionConfig(step);
                    walker.Walk();
                }
            }
        }

        public void Reject()
        {
            // Flush data and walk again to fill ActionConfigWalker.Log and ActionConfigWalker.Path.
            log = new StringBuilder();
            path = new List<string>();
            Walk();

            foreach (string actionCode in path)
            {
                Helpers.UndoAction(Document, actionCode);
            }
        }

        public override string ToString()
        {
            string summary = ActionInfo;
            summary += " (";
            summary += IsDecision ? "Karar," : "";
            summary += HasConditionalSplit ? "Koşullu Dallanma," : "";
            summary += HasParallelActions ? "Paralel," : "";
            summary += IsGroupByAND ? "VE," : "";
            summary += IsGroupByOR ? "VEYA," : "";
            summary += "Tamamlan" + (IsCompleted ? "dı" : "madı") + ",";
            summary += "Gerçekleş" + (DidItComeTrue ? "ti" : "medi") + ",";
            if(!summary.EndsWith("("))
                summary = summary.Substring(0, summary.Length - 1) + ")";
            return summary;
        } 
    }
}