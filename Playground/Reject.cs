﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Playground
{
    class Reject
    {
        #region OldRejectFields
        //public static string fromActionCode, toActionCode;
        //public static bool verbose;
        //public static string documentName;
        //public static string target;
        //public static List<string> path; 
        #endregion
        public static string documentName;

        public static OrganizationService _serviceProxy;

        private static bool delete = false;

        public static void Run()
        {
            Console.Write("Search for : ");
            documentName = Console.ReadLine();
            vrp_investdocument document = Helpers.GetDocument(documentName);
            try
            {
                Vrp_ActionConfigWalker walker = new Vrp_ActionConfigWalker(document, Helpers.GetActionConfig("1.6"));
                walker.Reject();
                Console.WriteLine(Vrp_ActionConfigWalker.Log);
                if (delete) _serviceProxy.Delete(vrp_investdocument.EntityLogicalName, document.Id);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return;
            }
        }

        #region OldRejectCodes

        //private static void UndoAllRelatedActionsWithinRange(vrp_investdocument document, string fromActionCode, string toActionCode)
        //{
        //    Console.ReadLine();

        //    Console.Write("* " + fromActionCode + " : ");
        //    if (path.Contains(fromActionCode))
        //    {
        //        Console.WriteLine("Skipped because this node was already undone");
        //        return;
        //    }
        //    string[] targets = toActionCode.Split('*');
        //    vrp_actionconfig config = GetActionConfig(fromActionCode);
        //    string fieldInfo = (config != null && (config.Attributes.Keys.Contains("vrp_fieldinfo"))) ? config.vrp_fieldinfo : "vrp_" + String.Join("_", fromActionCode.Split('.'));

        //    OptionSetValue actionFieldValue = null;
        //    if (document.Attributes.Keys.Contains(fieldInfo) && document[fieldInfo] != null)
        //    {
        //        actionFieldValue = ((OptionSetValue)document[fieldInfo]);
        //    }
        //    else
        //    {
        //        Console.WriteLine("Skipped because field '{0}' does not exist or has no value in the document.", fieldInfo);
        //        return;
        //    }

        //    bool completed = (actionFieldValue.Value.Equals(1)) ? false : true;

        //    if (config != null)
        //    {
        //        // Is this a decision?
        //        if (config.Attributes.ContainsKey("vrp_decision") && config["vrp_decision"] != null)
        //        {
        //            if (config.Attributes.ContainsKey("vrp_conditionalsplit") && config["vrp_conditionalsplit"] != null && config["vrp_conditionalsplit"].Equals(true))
        //            {
        //                Console.Write(" (conditional!) ");
        //                OptionSetValue decisionFieldValue = null;
        //                string decisionField = config.vrp_decision;
        //                if (document.Attributes.Keys.Contains(decisionField) && document[decisionField] != null)
        //                {
        //                    decisionFieldValue = ((OptionSetValue)document[decisionField]);
        //                    string[] decisionFieldValues = config.vrp_decisionvalue.Split('*');
        //                    int index = Array.IndexOf(decisionFieldValues, decisionFieldValue.Value.ToString());
        //                    if (index >= 0 && index < decisionFieldValues.Length)
        //                    {
        //                        string nextStep = config.vrp_nextstepcodeifitcomestrue.Split('*').ElementAt(index);
        //                        Console.Write("-> Next Step : " + nextStep);
        //                        Console.WriteLine();
        //                        UndoAllRelatedActionsWithinRange(document, nextStep, toActionCode);
        //                        return;
        //                    }
        //                    else
        //                    {
        //                        Console.WriteLine("Returning because there is no choice number #{0}", index);
        //                        return;
        //                    }
        //                }
        //                else
        //                {
        //                    Console.WriteLine("Returning because {0} does not exist on the document or has no value.", decisionField);
        //                    return;
        //                }
        //            }
        //            else
        //            {
        //                Console.Write(" (decision!) ");
        //                string[] decisionFields = config.vrp_decision.Split('*');
        //                string[] decisionFieldValues = config.vrp_decisionvalue.Split('*');
        //                for (int i = 0; i < decisionFields.Length; i++)
        //                {
        //                    string decisionField = decisionFields[i];
        //                    OptionSetValue decisionFieldValue = null;
        //                    if (document.Attributes.Keys.Contains(decisionField) && document[decisionField] != null)
        //                    {
        //                        string nextStep;
        //                        decisionFieldValue = ((OptionSetValue)document[decisionField]);
        //                        if (decisionFieldValue.Value.ToString().Equals(decisionFieldValues[i]))
        //                            nextStep = config.vrp_nextstepcodeifitcomestrue.Split('*').ElementAt(i);
        //                        else
        //                            nextStep = config.vrp_nextstepcodeifitdoesntcometrue.Split('*').ElementAt(i);

        //                        Console.Write("-> Next Step : " + nextStep);
        //                        Console.WriteLine();
        //                        UndoAllRelatedActionsWithinRange(document, nextStep, toActionCode);
        //                        return;
        //                    }
        //                    else
        //                    {
        //                        Console.WriteLine("Returning because {0} does not exist on the document or has no value.", decisionField);
        //                        return;
        //                    }
        //                }
        //            }
        //        }

        //        /*

        //        // TODO: Does this have any preconditions?

        //        */

        //        // TODO: Does this have any parallel tasks to wait for?
        //        if (config.Attributes.ContainsKey("vrp_paralelfield") && config["vrp_paralelfield"] != null)
        //        {
        //            string[] paralelFields = config.vrp_paralelfield.Split('*');
        //            string[] paralelFieldValues = config.vrp_paralelfieldvalue.Split('*');
        //            for (int i = 0; i < paralelFieldValues.Length && paralelFieldValues.Length == paralelFields.Length; i++)
        //            {
        //                if ((!document.Attributes.ContainsKey(paralelFields[i])) ||
        //                    document.GetAttributeValue(paralelFields[i]).ToString().Equals(paralelFieldValues[i])) break;
        //                Console.WriteLine("Returning because parallel fields were not set as expected.");
        //                return;
        //            }
        //        }



        //        string[] nextSteps;
        //        if (completed)
        //        {
        //            if (config.Attributes.Keys.Contains("vrp_nextstepcodeifitcomestrue"))
        //            {
        //                nextSteps = config.vrp_nextstepcodeifitcomestrue.Split('*');
        //                string[] intersect = targets.Intersect(nextSteps).ToArray();
        //                if (intersect.Length > 0)
        //                {
        //                    UndoAction(document, fromActionCode);
        //                    Console.WriteLine("Returning because '{0}' contains '{1}'.", toActionCode, intersect[0]);
        //                    return;
        //                }
        //            }
        //            else
        //            {
        //                Console.WriteLine("Returning because field was null '{0}'.", "vrp_nextstepcodeifitcomestrue");
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            if (config.Attributes.Keys.Contains("vrp_nextstepcodeifitdoesntcometrue"))
        //            {
        //                nextSteps = config.vrp_nextstepcodeifitdoesntcometrue.Split('*');
        //                string[] intersect = targets.Intersect(nextSteps).ToArray();
        //                if (intersect.Length > 0)
        //                {
        //                    UndoAction(document, fromActionCode);
        //                    Console.WriteLine("Returning because '{0}' contains '{1}'.", toActionCode, intersect[0]);
        //                    return;
        //                }
        //            }
        //            else
        //            {
        //                Console.WriteLine("Returning because field was null '{0}'.", "vrp_nextstepcodeifitdoesntcometrue");
        //                return;
        //            }
        //        }

        //        Console.WriteLine(((completed) ? "(completed)" : "(not completed)"));
        //        Console.Write("-> Next Steps : ");
        //        foreach (string stepCode in nextSteps)
        //            Console.Write(stepCode + ", ");
        //        Console.WriteLine();
        //        foreach (string stepCode in nextSteps)
        //            UndoAllRelatedActionsWithinRange(document, stepCode, toActionCode);

        //        UndoAction(document, fromActionCode);
        //    }
        //}

        //private static void UndoAction(vrp_investdocument document, string actionCode)
        //{
        //    Console.ReadLine();

        //    path.Add(actionCode);

        //    vrp_actionconfig config = GetActionConfig(actionCode);

        //    // Get field lists
        //    List<string> fieldLists = new List<string>(new string[] {
        //        config.vrp_fieldinfo, 
        //        config.vrp_otherfields
        //        //config.vrp_preconditions
        //        //"vrp_preconditions"
        //    });
        //    if (config.Attributes.ContainsKey("vrp_preconditions"))
        //        fieldLists.Add("vrp_preconditions");
        //    if (config.Attributes.ContainsKey("vrp_paralelfield"))
        //        fieldLists.Add(config.vrp_paralelfield);

        //    // Loop through field lists and set all fields to null.
        //    foreach (string fieldList in fieldLists)
        //        foreach (string field in fieldList.Split('*'))
        //            if (document.Attributes.ContainsKey(field))
        //                document[field] = null;
        //    document.vrp_taskId = actionCode;
        //    // Update document entity.
        //    _serviceProxy.Update(document);
        //    Console.WriteLine("{0} action undone.", actionCode);

        //    // Delete associated action record.
        //    vrp_action action = GetAction(actionCode, document);
        //    _serviceProxy.Delete(action.LogicalName, action.Id);
        //    Console.WriteLine("{0} action record deleted.", actionCode);

        //}

        //private static vrp_investdocument GetDocument(string documentName)
        //{
        //    QueryExpression getDocumentByName = new QueryExpression(vrp_investdocument.EntityLogicalName);
        //    getDocumentByName.ColumnSet = new ColumnSet(true);
        //    getDocumentByName.Criteria.AddCondition(new ConditionExpression("vrp_name", ConditionOperator.Equal, documentName));
        //    return _serviceProxy.RetrieveMultiple(getDocumentByName).Entities.FirstOrDefault().ToEntity<vrp_investdocument>();
        //}

        //private static vrp_action GetAction(string actionCode, vrp_investdocument document)
        //{
        //    QueryExpression findAction = new QueryExpression(vrp_action.EntityLogicalName);
        //    findAction.ColumnSet = new ColumnSet(true);
        //    findAction.Criteria.AddCondition(new ConditionExpression("regardingobjectid", ConditionOperator.Equal, document.Id));
        //    findAction.Criteria.AddCondition(new ConditionExpression("vrp_actioncode", ConditionOperator.Equal, actionCode));
        //    try
        //    {
        //        return _serviceProxy.RetrieveMultiple(findAction).Entities.FirstOrDefault().ToEntity<vrp_action>();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("GetAction is returning null due to an exception : " + e.Message);
        //        return null;
        //    }
        //}

        //public static vrp_actionconfig GetActionConfig(string actionCode)
        //{
        //    QueryExpression getConfigByActionCode = new QueryExpression(vrp_actionconfig.EntityLogicalName);
        //    getConfigByActionCode.ColumnSet = new ColumnSet(true);
        //    getConfigByActionCode.Criteria.AddCondition(new ConditionExpression("vrp_actioncode", ConditionOperator.Equal, actionCode));
        //    try
        //    {
        //        return _serviceProxy.RetrieveMultiple(getConfigByActionCode).Entities.FirstOrDefault().ToEntity<vrp_actionconfig>();

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("GetActionConfig is returning null due to an exception : " + e.Message);
        //        return null;
        //    }
        //}

        //public static void BrowseConfig(string start)
        //{
        //    vrp_actionconfig config = GetActionConfig(start);
        //    Console.WriteLine(start + ":");
        //    if (config.vrp_nextstepcodeifitcomestrue != null)
        //    {
        //        string[] nextSteps = config.vrp_nextstepcodeifitcomestrue.Split('*');
        //        Console.Write("\t p : ");
        //        for (int i = 0; i < nextSteps.Length; i++)
        //        {
        //            Console.Write(nextSteps[i] + ((i == nextSteps.Length - 1) ? "\n" : ","));
        //        }
        //    }
        //    else
        //    {
        //        Console.WriteLine("\t p : none");
        //    }
        //    if (config.vrp_nextstepcodeifitdoesntcometrue != null)
        //    {
        //        string[] nextSteps = config.vrp_nextstepcodeifitdoesntcometrue.Split('*');
        //        Console.Write("\t n : ");
        //        for (int i = 0; i < nextSteps.Length; i++)
        //        {
        //            Console.Write(nextSteps[i] + ((i == nextSteps.Length - 1) ? "\n" : ","));
        //        }
        //    }
        //    else
        //    {
        //        Console.WriteLine("\t n : none");
        //    }
        //    if (Console.ReadKey(true).Equals(new ConsoleKeyInfo('p', ConsoleKey.P, false, false, false)))
        //    {
        //        if (config.vrp_nextstepcodeifitcomestrue != null)
        //        {
        //            string[] nextSteps = config.vrp_nextstepcodeifitcomestrue.Split('*');
        //            for (int i = 0; i < nextSteps.Length; i++)
        //            {
        //                BrowseConfig(nextSteps[i]);
        //            }
        //        }
        //        else
        //        {
        //            Console.WriteLine("\t p : none");
        //        }
        //    }
        //    else
        //    {
        //        if (config.vrp_nextstepcodeifitdoesntcometrue != null)
        //        {
        //            string[] nextSteps = config.vrp_nextstepcodeifitdoesntcometrue.Split('*');
        //            for (int i = 0; i < nextSteps.Length; i++)
        //            {
        //                BrowseConfig(nextSteps[i]);
        //            }
        //        }
        //        else
        //        {
        //            Console.WriteLine("\t n : none");
        //        }
        //    }
        //} 
        #endregion

    }
}
