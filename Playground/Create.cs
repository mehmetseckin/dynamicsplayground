﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Playground
{
    static class Create
    {
        public static OrganizationService _serviceProxy;
        private static bool delete = false;

        public static void Run()
        {
            Console.WriteLine("Creating a new document...");
           Guid docId = _serviceProxy.Create(new vrp_investdocument());
            Thread.Sleep(3000);
            //Guid docId = Guid.Parse("{935AD49C-0552-E411-80CB-000C29D60B07}");

            vrp_investdocument document = _serviceProxy.Retrieve(vrp_investdocument.EntityLogicalName, docId, new ColumnSet(true)).ToEntity<vrp_investdocument>();
            Vrp_ActionConfigWalker walker = new Vrp_ActionConfigWalker(document, Helpers.GetActionConfig("1.1"));
            walker.PromptAndWalk();
            if (delete) _serviceProxy.Delete(vrp_investdocument.EntityLogicalName, docId);
        }


    }
}
