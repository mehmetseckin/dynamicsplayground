﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Net;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Threading;
using Microsoft.VisualBasic.FileIO;


namespace Playground
{
    internal class Program
    {
        private static OrganizationService _serviceProxy;
        private static string connectionString;
        private static bool verbose;

        private static void Init(string[] args)
        {
            if (args.Length < 4)
            {
                PromptCredentials();
            }

            connectionString = String.Format("Url={0}; Username={1}\\{2}; Password={3};", args[0], args[1], args[2], args[3]);
            verbose = args[4].Equals("true");
        }

        private static void Main(string[] args)
        {
            Init(args);
            try
            {
                // This using statement assures that the service proxy will be disposed properly.
                using (_serviceProxy = new OrganizationService(CrmConnection.Parse(connectionString)))
                {
                    // Pass service proxy to the classes
                    // Will change. Later.
                    
                    Helpers._serviceProxy = Reject._serviceProxy = Create._serviceProxy = _serviceProxy;
                    

                    #region Retrieve user information
                    Guid userId = ((WhoAmIResponse)_serviceProxy.Execute(new WhoAmIRequest())).UserId;
                    SystemUser user = _serviceProxy.Retrieve("systemuser", userId, new ColumnSet(true)).ToEntity<SystemUser>();
                    Console.WriteLine("Logged in as {0}.", user.FullName);
                    #endregion Retrieve user information   
                    



                    #region Delete DCFs and DCF Informations. 
                    /*
                    QueryExpression query = new QueryExpression("vrp_dcfdetail");
                    EntityCollection ec = _serviceProxy.RetrieveMultiple(query);
                    int i = 0;
                    if (ec.Entities.Count > 0)
                    {
                        foreach (Entity e in ec.Entities)
                        {
                            DeleteRequest dr = new DeleteRequest()
                            {
                                Target = new EntityReference(e.LogicalName, e.Id)
                            };
                            _serviceProxy.Execute(dr);
                            i++;
                            Console.Write("\r{0} of {1} details deleted.                                ", i, ec.Entities.Count);
                        }
                    }

                    Console.WriteLine();
                    Console.WriteLine("----");
                    Console.WriteLine();

                    query = new QueryExpression("vrp_dcf");
                    ec = _serviceProxy.RetrieveMultiple(query);
                    i = 0;
                    if (ec.Entities.Count > 0)
                    {
                        foreach (Entity e in ec.Entities)
                        {
                            DeleteRequest dr = new DeleteRequest()
                            {
                                Target = new EntityReference(e.LogicalName, e.Id)
                            };
                            _serviceProxy.Execute(dr);
                            i++;
                            Console.Write("\r{0} of {1} headers deleted.                                ", i, ec.Entities.Count);
                        }
                    }
                    
                    */
                    #endregion Delete DCFs and DCF Informations.

                    #region Create a custom global dataset
                    //int _languageCode = 1055;
                    //int _min = 168260000;
                    //string _optionSetName = "vrp_cities";

                    //Dictionary<string, int> cities = new Dictionary<string, int>();
                    //using (TextFieldParser parser = new TextFieldParser("cities.csv"))
                    //{
                    //    parser.TextFieldType = FieldType.Delimited;
                    //    parser.SetDelimiters(";");
                    //    while (!parser.EndOfData)
                    //    {
                    //        string[] fields = parser.ReadFields();
                    //        cities.Add(fields[1], _min + Convert.ToInt32(fields[2])); // add the minimum optset value to the city code (e.g ADANA: 168260000 + 1)
                    //    }
                    //}

                    //OptionSetMetadata setupOptionSetMetadata = new OptionSetMetadata()
                    //{
                    //    // Unique name of the optionset
                    //    Name = _optionSetName,
                    //    DisplayName = new Label("Şehirler", _languageCode),
                    //    Description = new Label("Türkiye'deki tüm şehirler", _languageCode),
                    //    IsGlobal = true,
                    //    OptionSetType = OptionSetType.Picklist,
                    //};

                    //// Populate optionset
                    //OptionMetadataCollection cityOptions = new OptionMetadataCollection();
                    //foreach (var item in cities)
                    //{
                    //    setupOptionSetMetadata.Options.Add(new OptionMetadata(new Label(item.Key, _languageCode), item.Value));
                    //}

                    //// Wrap the OptionSetMetadata in the appropriate request.
                    //CreateOptionSetRequest createOptionSetRequest = new CreateOptionSetRequest
                    //{
                    //    OptionSet = setupOptionSetMetadata
                    //};

                    //// Pass the execute statement to the CRM service.
                    //_serviceProxy.Execute(createOptionSetRequest); 
                    #endregion
 
                    #region Update document descriptions.
                    //QueryExpression retrieveDocumentsQuery = new QueryExpression("vrp_investdocument");
                    //retrieveDocumentsQuery.ColumnSet = new ColumnSet(new string[] { "vrp_investdocumentid", "vrp_taskid", "vrp_taskdescription", "vrp_name" });
                    //retrieveDocumentsQuery.Criteria.AddCondition(new ConditionExpression("vrp_investdocumentid", ConditionOperator.Equal, new Guid("{F5C459EC-A45A-E411-80C7-005056940A09}")));
                    //foreach (Entity entity in _serviceProxy.RetrieveMultiple(retrieveDocumentsQuery).Entities)
                    //{
                    //    string[] actionCodes = entity["vrp_taskid"].ToString().Split('*');
                    //    string[] description = new string[actionCodes.Length];

                    //    for (int i = 0; i < actionCodes.Length; i++)
                    //    {
                    //        description[i] = Helpers.GetActionConfig(actionCodes[i]).vrp_description.ToString();
                    //    }

                    //    string taskdescription = String.Join("*", description);

                    //    if (entity.Attributes.Contains("vrp_taskdescription"))
                    //        entity.Attributes["vrp_taskdescription"] = taskdescription;
                    //    else
                    //        entity.Attributes.Add("vrp_taskdescription", taskdescription);
                    //    Console.WriteLine("Updating description for {0} : {1}", entity["vrp_name"], entity["vrp_taskdescription"]);
                    //    _serviceProxy.Update(entity);
                    //} 
                    #endregion

                    #region GetOpenActions
                    //QueryExpression getOpenActions = new QueryExpression(vrp_action.EntityLogicalName);

                    //ConditionExpression isOpen = new ConditionExpression("statecode", ConditionOperator.Equal, (int)vrp_actionState.Open);
                    //ConditionExpression isDue = new ConditionExpression("vrp_estimatedfinishdate", ConditionOperator.OnOrBefore, DateTime.Now.AddDays(-1));

                    //LinkEntity actionSLALog = new LinkEntity(vrp_action.EntityLogicalName, vrp_slalog.EntityLogicalName, "vrp_slalogid", "vrp_slalogid", JoinOperator.Inner);
                    //actionSLALog.EntityAlias = "slalog";
                    //actionSLALog.Columns = new ColumnSet(true);
                    //actionSLALog.LinkCriteria.AddCondition(isDue);
                    //getOpenActions.LinkEntities.Add(actionSLALog);

                    //getOpenActions.ColumnSet = new ColumnSet(true);
                    //getOpenActions.Criteria.AddCondition(isOpen);
                    //EntityCollection ec = _serviceProxy.RetrieveMultiple(getOpenActions); 
                    #endregion

                    #region Retrieve Deleted Record from Audit
                   
                    #region 3645-KIRBASLAR
                    // Silinen Yatırım Dosyası Audit : 
                    // ["A80A397E-FC7B-E411-8C19-C4346BC0D434",
                    // Silinen Aksiyon Auditleri : 
                    //["A80A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"A90A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"AA0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"AB0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"AC0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"AD0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"AE0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"AF0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"B00A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"B10A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"B20A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"B30A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"B40A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"B50A397E-FC7B-E411-8C19-C4346BC0D434",
                    //"B60A397E-FC7B-E411-8C19-C4346BC0D434"] 

                    //string[] auditIds = 
                    //    {
                    //        "A80A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "A90A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "AA0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "AB0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "AC0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "AD0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "AE0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "AF0A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "B00A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "B10A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "B20A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "B30A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "B40A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "B50A397E-FC7B-E411-8C19-C4346BC0D434",
                    //        "B60A397E-FC7B-E411-8C19-C4346BC0D434"
                    //    };
                    #endregion

                    #region BASMAKCI-BLD
                    //--Başmakçı Son Hali
                    //--6B7057E9-3581-E411-8C19-C4346BC0D434
                    //--Silinen Aksiyon Kayıtları
                    // "F0C70E09-3681-E411-8C19-C4346BC0D434",
                    // "EEC70E09-3681-E411-8C19-C4346BC0D434",
                    // "ECC70E09-3681-E411-8C19-C4346BC0D434",
                    // "EAC70E09-3681-E411-8C19-C4346BC0D434",
                    // "E8C70E09-3681-E411-8C19-C4346BC0D434",
                    // "E6C70E09-3681-E411-8C19-C4346BC0D434",
                    // "E3C4B902-3681-E411-8C19-C4346BC0D434",
                    // "E1C4B902-3681-E411-8C19-C4346BC0D434",
                    // "DEC4B902-3681-E411-8C19-C4346BC0D434",
                    // "DBC4B902-3681-E411-8C19-C4346BC0D434",
                    // "D8C4B902-3681-E411-8C19-C4346BC0D434",
                    // "04A12BFC-3581-E411-8C19-C4346BC0D434",
                    // "03A12BFC-3581-E411-8C19-C4346BC0D434",
                    // "FCA02BFC-3581-E411-8C19-C4346BC0D434",
                    // "F9A02BFC-3581-E411-8C19-C4346BC0D434",
                    // "B98FF5F5-3581-E411-8C19-C4346BC0D434",
                    // "B68FF5F5-3581-E411-8C19-C4346BC0D434",
                    // "B38FF5F5-3581-E411-8C19-C4346BC0D434",
                    // "ED03C9EF-3581-E411-8C19-C4346BC0D434",
                    // "EB03C9EF-3581-E411-8C19-C4346BC0D434",
                    // "E903C9EF-3581-E411-8C19-C4346BC0D434",
                    // "E703C9EF-3581-E411-8C19-C4346BC0D434",
                    // "E503C9EF-3581-E411-8C19-C4346BC0D434",
                    // "E303C9EF-3581-E411-8C19-C4346BC0D434",
                    // "E103C9EF-3581-E411-8C19-C4346BC0D434",
                    // "747057E9-3581-E411-8C19-C4346BC0D434",
                    // "727057E9-3581-E411-8C19-C4346BC0D434"
                    //string[] auditIds = 
                    //    {
                    //         "F0C70E09-3681-E411-8C19-C4346BC0D434",
                    //         "EEC70E09-3681-E411-8C19-C4346BC0D434",
                    //         "ECC70E09-3681-E411-8C19-C4346BC0D434",
                    //         "EAC70E09-3681-E411-8C19-C4346BC0D434",
                    //         "E8C70E09-3681-E411-8C19-C4346BC0D434",
                    //         "E6C70E09-3681-E411-8C19-C4346BC0D434",
                    //         "E3C4B902-3681-E411-8C19-C4346BC0D434",
                    //         "E1C4B902-3681-E411-8C19-C4346BC0D434",
                    //         "DEC4B902-3681-E411-8C19-C4346BC0D434",
                    //         "DBC4B902-3681-E411-8C19-C4346BC0D434",
                    //         "D8C4B902-3681-E411-8C19-C4346BC0D434",
                    //         "04A12BFC-3581-E411-8C19-C4346BC0D434",
                    //         "03A12BFC-3581-E411-8C19-C4346BC0D434",
                    //         "FCA02BFC-3581-E411-8C19-C4346BC0D434",
                    //         "F9A02BFC-3581-E411-8C19-C4346BC0D434",
                    //         "B98FF5F5-3581-E411-8C19-C4346BC0D434",
                    //         "B68FF5F5-3581-E411-8C19-C4346BC0D434",
                    //         "B38FF5F5-3581-E411-8C19-C4346BC0D434",
                    //         "ED03C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "EB03C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E903C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E703C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E503C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E303C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E103C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "747057E9-3581-E411-8C19-C4346BC0D434",
                    //         "727057E9-3581-E411-8C19-C4346BC0D434"
                    //    };
                    #endregion


                    //string basmakciSnapshot = "6B7057E9-3581-E411-8C19-C4346BC0D434";
                    //string[] auditIds = 
                    //    {
                    //         "F0C70E09-3681-E411-8C19-C4346BC0D434",
                    //         "EEC70E09-3681-E411-8C19-C4346BC0D434",
                    //         "ECC70E09-3681-E411-8C19-C4346BC0D434",
                    //         "EAC70E09-3681-E411-8C19-C4346BC0D434",
                    //         "E8C70E09-3681-E411-8C19-C4346BC0D434",
                    //         "E6C70E09-3681-E411-8C19-C4346BC0D434",
                    //         "E3C4B902-3681-E411-8C19-C4346BC0D434",
                    //         "E1C4B902-3681-E411-8C19-C4346BC0D434",
                    //         "DEC4B902-3681-E411-8C19-C4346BC0D434",
                    //         "DBC4B902-3681-E411-8C19-C4346BC0D434",
                    //         "D8C4B902-3681-E411-8C19-C4346BC0D434",
                    //         "04A12BFC-3581-E411-8C19-C4346BC0D434",
                    //         "03A12BFC-3581-E411-8C19-C4346BC0D434",
                    //         "FCA02BFC-3581-E411-8C19-C4346BC0D434",
                    //         "F9A02BFC-3581-E411-8C19-C4346BC0D434",
                    //         "B98FF5F5-3581-E411-8C19-C4346BC0D434",
                    //         "B68FF5F5-3581-E411-8C19-C4346BC0D434",
                    //         "B38FF5F5-3581-E411-8C19-C4346BC0D434",
                    //         "ED03C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "EB03C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E903C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E703C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E503C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E303C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "E103C9EF-3581-E411-8C19-C4346BC0D434",
                    //         "747057E9-3581-E411-8C19-C4346BC0D434",
                    //         "727057E9-3581-E411-8C19-C4346BC0D434"
                    //    };

                    //int counter = 0;
                    ////Recreate records.
                    //foreach (string auditIdstr in auditIds)
                    //{
                    //    try
                    //    {
                    //        Guid auditId = new Guid(auditIdstr);
                    //        RetrieveAuditDetailsRequest request = new RetrieveAuditDetailsRequest();
                    //        request.AuditId = auditId;
                    //        RetrieveAuditDetailsResponse response = (RetrieveAuditDetailsResponse)_serviceProxy.Execute(request);
                    //        Entity e = ((AttributeAuditDetail)response.AuditDetail).OldValue;
                    //        e["statuscode"] = new OptionSetValue(1);
                    //        counter++;
                    //        _serviceProxy.Create(e);
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        counter--;
                    //        continue;
                    //    }
                    //} 

                    //Guid auditId = new Guid(basmakciSnapshot);
                    //RetrieveAuditDetailsRequest request = new RetrieveAuditDetailsRequest();
                    //request.AuditId = auditId;
                    //RetrieveAuditDetailsResponse response = (RetrieveAuditDetailsResponse)_serviceProxy.Execute(request);
                    //Entity e = ((AttributeAuditDetail)response.AuditDetail).OldValue;
                    //Console.WriteLine("Updating entity : " + (e.Attributes.ContainsKey("vrp_name") ? e["vrp_name"] : e["subject"]));

                    //foreach (KeyValuePair<String, Object> attribute in e.Attributes)
                    //{
                    //    Console.WriteLine(attribute.Key + ": " + attribute.Value);
                    //}

                    //_serviceProxy.Update(e);  
                    #endregion
                    #region Queue to Role

                    //string[] queueNames = {
                    //    "Accounting",
                    //    "Accounting Manager",
                    //    "Asset Coordinator",
                    //    "Asset Manager",
                    //    "Asset Operational Support",
                    //    "Asset Team Leader",
                    //    "Automatic Manager",
                    //    "Automation Team Leader",
                    //    "Controlling(PO) / Controlling (OMV)",
                    //    "Credit Management",
                    //    "Customer Service",
                    //    "Deputy CEO",
                    //    "Divisional CFO",
                    //    "EC Secretary",
                    //    "Engineering",
                    //    "Engineering Manager",
                    //    "Environmental Compliance",
                    //    "Fuel Sales",
                    //    "HSSE",
                    //    "Indoor Support",
                    //    "Internal Control",
                    //    "Legal",
                    //    "LPG/Performance Planning",
                    //    "Negotiator",
                    //    "NFC",
                    //    "NOB",
                    //    "Performance Planning",
                    //    "Retail Director",
                    //    "Sales Management",
                    //    "Special Project Manager",
                    //    "Tax"
                    //    };

                    //foreach(string queueName in queueNames)
                    //{
                    //    Queue queue = Helpers.GetQueueByName(queueName);
                    //    Role role = Helpers.GetRoleByName(queueName);
                    //    if (queue != null && role != null)
                    //    {
                    //        foreach (SystemUser qUser in Helpers.GetUsersByQueue(queue.Id))
                    //        {
                    //            if (verbose) Console.WriteLine("Associating '{0}' with role '{1}'.", qUser.FullName, role.Name);
                    //            Helpers.AssociateUserWithRole(role, qUser);
                    //        }
                    //    }
                    //} 
                    #endregion

                }
            }
            catch (Exception mse)
            {
                Console.WriteLine("An error occured : {0}", mse.Message);
                Console.WriteLine("Stack Trace : {0}", mse.StackTrace);
                if (mse.InnerException != null)
                    Console.WriteLine("Details : {0}", mse.InnerException.Message);
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadLine();
        }

        #region Prompt methods

        private static void PromptAssociateQueue(SystemUser user, Queue q)
        {
            Console.WriteLine("Do you want to associate {0} with {1} queue ? (y/n) ", user.FullName, q.Name);
            string reply = Console.ReadLine();
            if (reply.Equals("y"))
            {
                Console.WriteLine("Attempting to associate {0} with {1} queue", user.FullName, q.Name);
                AssociateResponse resp = Helpers.AssociateUserWithQueue(q, user);
                Console.WriteLine("{0}", (resp.ResponseName == null) ? "Failed to associate." : "Done.");
            }
        }

        private static void PromptAssociateRole(SystemUser user, Role role)
        {
            Console.WriteLine("Do you want to associate {0} with {1} role? (y/n) ", user.FullName, role.Name);
            string reply = Console.ReadLine();
            if (reply.Equals("y"))
            {
                Console.WriteLine("Attempting to associate {0} with {1} role", user.FullName, role.Name);
                Helpers.AssociateUserWithRole(role, user);
            }
        }

        private static void PromptCredentials()
        {
            string server, domain, username, password;
            Console.WriteLine("Please provide your credentials.");
            Console.Write("Server Name : ");
            server = Console.ReadLine();
            Console.Write("Domain : ");
            domain = Console.ReadLine();
            Console.Write("Username : ");
            username = Console.ReadLine();
            Console.Write("Password : ");
            password = Console.ReadLine();
            Console.Write("Verbose? (true/false) : ");
            verbose = (Console.ReadLine()).Equals("true");

            connectionString = String.Format("Url={0}; Username={1}\\{2}; Password={3};", server, domain, username, password);
        }

        private static void PromptDisassociateQueue(SystemUser user, Queue q)
        {
            Console.WriteLine("Disassociate {0} from {1} queue? (y/n) ", user.FullName, q.Name);
            string reply = Console.ReadLine();
            if (reply.Equals("y"))
            {
                Console.WriteLine("Attempting to disassociate {0} from {1} queue", user.FullName, q.Name);
                DisassociateResponse resp = Helpers.DisassociateUserFromQueue(q, user);
                Console.WriteLine("{0}", (resp.ResponseName == null) ? "Failed to disassociate." : "Done.");
            }
        }

        private static void PromptDisassociateRole(SystemUser user, Role role)
        {
            Console.WriteLine("Disassociate {0} from {1} role? (y/n) ", user.FullName, role.Name);
            string reply = Console.ReadLine();
            if (reply.Equals("y"))
            {
                Console.WriteLine("Attempting to disassociate {0} from {1} role", user.FullName, role.Name);
                DisassociateResponse resp = Helpers.DisassociateUserFromRole(role, user);
                Console.WriteLine("{0}", (resp.ResponseName == null) ? "Failed to disassociate." : "Done.");
            }
        }

        #endregion Prompt methods


    }
}