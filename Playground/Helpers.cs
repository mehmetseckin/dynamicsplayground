﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Playground
{
    internal class Helpers
    {
        public static OrganizationService _serviceProxy;

        #region Helper methods for CRM

        /// <summary>
        /// Adds an user to a queue.
        /// </summary>
        /// <param name="queue">The queue to add the user</param>
        /// <param name="user">The user to be added</param>
        /// <returns>The service response is returned.</returns>
        public static AddToQueueResponse AddUserToQueue(Queue queue, SystemUser user)
        {
            AddToQueueRequest addRequest = new AddToQueueRequest
            {
                Target = new EntityReference(SystemUser.EntityLogicalName, user.Id),
                DestinationQueueId = queue.Id
            };

            AddToQueueResponse response = (AddToQueueResponse)_serviceProxy.Execute(addRequest);
            return response;
        }

        /// <summary>
        /// Associates a user with a queue.
        /// </summary>
        /// <param name="queue">The queue to add the user</param>
        /// <param name="user">The user to be added</param>
        /// <returns>The service response is returned.</returns>
        public static AssociateResponse AssociateUserWithQueue(Queue queue, SystemUser user)
        {
            AssociateRequest userWithQueue = new AssociateRequest
            {
                Target = new EntityReference(SystemUser.EntityLogicalName, user.Id),
                RelatedEntities = new EntityReferenceCollection
                {
                    new EntityReference(Queue.EntityLogicalName, queue.Id)
                },
                Relationship = new Relationship("queuemembership_association")
            };

            //_serviceProxy.Associate(
            //                SystemUser.EntityLogicalName,
            //                user.Id,
            //                new Relationship("queuemembership_association"),
            //                new EntityReferenceCollection
            //                {
            //                    new EntityReference(Queue.EntityLogicalName, queue.Id)
            //                });

            return (AssociateResponse)_serviceProxy.Execute(userWithQueue);
        }

        /// <summary>
        /// Associates the user with the given role.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="user"></param>
        public static AssociateResponse AssociateUserWithRole(Role role, SystemUser user)
        {
            AssociateRequest userWithRole = new AssociateRequest
            {
                Target = new EntityReference(SystemUser.EntityLogicalName, user.Id),
                Relationship = new Relationship("systemuserroles_association"),
                RelatedEntities = new EntityReferenceCollection() { new EntityReference(Role.EntityLogicalName, role.Id) }
            };

            AssociateResponse response = (AssociateResponse)_serviceProxy.Execute(userWithRole);
            return response;
        }

        /// <summary>
        /// Disassociates a user from a queue using the N:N relationship between systemuser and queue
        /// </summary>
        /// <param name="queue">The queue to add the user</param>
        /// <param name="user">The user to be added</param>
        /// <returns>The service response is returned.</returns>
        public static DisassociateResponse DisassociateUserFromQueue(Queue queue, SystemUser user)
        {
            DisassociateRequest userFromQueue = new DisassociateRequest
            {
                Target = new EntityReference(SystemUser.EntityLogicalName, user.Id),
                RelatedEntities = new EntityReferenceCollection
                {
                    new EntityReference(Queue.EntityLogicalName, queue.Id)
                },
                Relationship = new Relationship("queuemembership_association")
            };

            //_serviceProxy.Disassociate(
            //                SystemUser.EntityLogicalName,
            //                user.Id,
            //                new Relationship("queuemembership_association"),
            //                new EntityReferenceCollection
            //                {
            //                    new EntityReference(Queue.EntityLogicalName, queue.Id)
            //                });

            return (DisassociateResponse)_serviceProxy.Execute(userFromQueue);
        }

        /// <summary>
        /// Disassociates a user from a role.
        /// </summary>
        /// <param name="role"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static DisassociateResponse DisassociateUserFromRole(Role role, SystemUser user)
        {
            DisassociateRequest userFromRole = new DisassociateRequest
            {
                Target = new EntityReference(SystemUser.EntityLogicalName, user.Id),
                RelatedEntities = new EntityReferenceCollection
                {
                    new EntityReference(Queue.EntityLogicalName, role.Id)
                },
                Relationship = new Relationship("systemuserroles_association")
            };

            return (DisassociateResponse)_serviceProxy.Execute(userFromRole);
        }

        /// <summary>
        /// Gets the Queue entity by the given name. Returns null if the queue does not exist.
        /// </summary>
        /// <param name="queueName">Queue name to search for.</param>
        /// <returns></returns>
        public static Queue GetQueueByName(string queueName)
        {
            ConditionExpression conditionName = new ConditionExpression(
                                    "name", ConditionOperator.Equal, queueName);

            FilterExpression filter = new FilterExpression();
            filter.AddCondition(conditionName);

            QueryExpression query = new QueryExpression(Queue.EntityLogicalName);
            query.Criteria.AddFilter(filter);
            query.ColumnSet.AllColumns = true;

            EntityCollection results = _serviceProxy.RetrieveMultiple(query);

            if (results.Entities.Any())
            {
                return results.Entities[0].ToEntity<Queue>();
            }

            // If we've come this far, something went wrong. Return null.
            return null;
        }

        /// <summary>
        /// Gets the role specified by its name.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public static Role GetRoleByName(string roleName)
        {
            Role _role = null;
            // Find the role.
            QueryExpression query = new QueryExpression
            {
                EntityName = Role.EntityLogicalName,
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression
                {
                    Conditions =
                {
                    new ConditionExpression
                    {
                        AttributeName = "name",
                        Operator = ConditionOperator.Equal,
                        Values = {roleName}
                    }
                }
                }
            };
            // Get the role.
            EntityCollection roles = _serviceProxy.RetrieveMultiple(query);
            if (roles.Entities.Count > 0)
            {
                _role = _serviceProxy.RetrieveMultiple(query).Entities[0].ToEntity<Role>();
            }

            return _role;
        }

        /// <summary>
        /// Retrieves queues of a user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<Queue> GetUserQueuesById(Guid userId)
        {
            List<Queue> queueList = new List<Queue>();

            QueryExpression query = new QueryExpression();
            query.EntityName = "queue";
            query.ColumnSet = new ColumnSet(true);

            LinkEntity queue = new LinkEntity();
            queue.LinkFromEntityName = "queue";
            queue.LinkFromAttributeName = "queueid";
            queue.LinkToEntityName = "queuemembership";
            queue.LinkToAttributeName = "queueid";

            LinkEntity userQueues = new LinkEntity();
            userQueues.LinkFromEntityName = "queuemembership";
            userQueues.LinkFromAttributeName = "systemuserid";
            userQueues.LinkToEntityName = "systemuser";
            userQueues.LinkToAttributeName = "systemuserid";

            ConditionExpression conditionExpression = new ConditionExpression();
            conditionExpression.AttributeName = "systemuserid";
            conditionExpression.Operator = ConditionOperator.Equal;
            conditionExpression.Values.Add(userId);

            userQueues.LinkCriteria = new FilterExpression();
            userQueues.LinkCriteria.Conditions.Add(conditionExpression);

            queue.LinkEntities.Add(userQueues);
            query.LinkEntities.Add(queue);

            EntityCollection queues = _serviceProxy.RetrieveMultiple(query);
            if (queues != null && queues.Entities.Count > 0)
            {
                foreach (Entity q in queues.Entities)
                {
                    queueList.Add(q.ToEntity<Queue>());
                }
            }

            return queueList;
        }

        /// <summary>
        /// Retrieves roles of a user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static List<Role> GetUserRolesById(Guid userId)
        {
            List<Role> roleList = new List<Role>();

            QueryExpression query = new QueryExpression();
            query.EntityName = "role";
            query.ColumnSet = new ColumnSet(true);
            query.Distinct = false; // We set this false, because there may be multiple roles with the same name for other organizations etc.

            LinkEntity role = new LinkEntity();
            role.LinkFromEntityName = "role";
            role.LinkFromAttributeName = "roleid";
            role.LinkToEntityName = "systemuserroles";
            role.LinkToAttributeName = "roleid";

            LinkEntity userRoles = new LinkEntity();
            userRoles.LinkFromEntityName = "systemuserroles";
            userRoles.LinkFromAttributeName = "systemuserid";
            userRoles.LinkToEntityName = "systemuser";
            userRoles.LinkToAttributeName = "systemuserid";

            // WHERE
            ConditionExpression conditionExpression = new ConditionExpression();
            conditionExpression.AttributeName = "systemuserid";
            conditionExpression.Operator = ConditionOperator.Equal;
            conditionExpression.Values.Add(userId);

            userRoles.LinkCriteria = new FilterExpression();
            userRoles.LinkCriteria.Conditions.Add(conditionExpression);

            role.LinkEntities.Add(userRoles);
            query.LinkEntities.Add(role);

            EntityCollection roles = _serviceProxy.RetrieveMultiple(query);
            if (roles != null && roles.Entities.Count > 0)
            {
                foreach (Entity r in roles.Entities)
                {
                    Console.WriteLine("Found role : {0}", r.GetAttributeValue("name"));
                    roleList.Add(r.ToEntity<Role>());
                }
            }

            return roleList;
        }

        /// <summary>
        /// Gets users associated with the specified queue.
        /// </summary>
        /// <param name="queueId"></param>
        /// <returns></returns>
        public static List<SystemUser> GetUsersByQueue(Guid queueId)
        {
            List<SystemUser> userList = new List<SystemUser>();

            QueryExpression query = new QueryExpression();
            query.EntityName = SystemUser.EntityLogicalName;
            query.ColumnSet = new ColumnSet(true);

            LinkEntity user = new LinkEntity();
            user.LinkFromEntityName = SystemUser.EntityLogicalName;
            user.LinkFromAttributeName = "systemuserid";
            user.LinkToEntityName = "queuemembership";
            user.LinkToAttributeName = "systemuserid";

            LinkEntity linkQueueSystemUser = new LinkEntity();
            linkQueueSystemUser.LinkFromEntityName = "queuemembership";
            linkQueueSystemUser.LinkFromAttributeName = "queueid";
            linkQueueSystemUser.LinkToEntityName = Queue.EntityLogicalName;
            linkQueueSystemUser.LinkToAttributeName = "queueid";

            ConditionExpression conditionExpression = new ConditionExpression();
            conditionExpression.AttributeName = "queueid";
            conditionExpression.Operator = ConditionOperator.Equal;
            conditionExpression.Values.Add(queueId);

            linkQueueSystemUser.LinkCriteria = new FilterExpression();
            linkQueueSystemUser.LinkCriteria.Conditions.Add(conditionExpression);

            user.LinkEntities.Add(linkQueueSystemUser);
            query.LinkEntities.Add(user);

            EntityCollection users = _serviceProxy.RetrieveMultiple(query);
            if (users != null && users.Entities.Count > 0)
            {
                foreach (Entity u in users.Entities)
                {
                    userList.Add(u.ToEntity<SystemUser>());
                }
            }

            return userList;
        }

        #endregion Helper methods for CRM Actions
        public static Role GetRoleByName(string roleName, Guid businessUnitId, IOrganizationService _serviceProxy = null)
        {
            Role _role = null;
            // Find the role.
            QueryExpression query = new QueryExpression
            {
                EntityName = Role.EntityLogicalName,
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression
                {
                    Conditions =
                {
                    new ConditionExpression
                    {
                        AttributeName = "name",
                        Operator = ConditionOperator.Equal,
                        Values = { roleName }
                    },
                    new ConditionExpression
                    {
                        AttributeName = "businessunitid",
                        Operator = ConditionOperator.Equal,
                        Values = { businessUnitId }
                    }
                }
                }
            };
            // Get the role.
            EntityCollection roles = _serviceProxy.RetrieveMultiple(query);
            if (roles.Entities.Count > 0)
            {
                _role = roles.Entities[0].ToEntity<Role>();
            }

            return _role;
        }
        public static bool HasRole(Guid systemUserId, Guid roleId, IOrganizationService _service = null)
        {
            QueryExpression query = new QueryExpression();
            query.EntityName = "role";
            query.ColumnSet = new ColumnSet("roleid");
            query.Distinct = false; // We set this false, because there may be multiple roles with the same name for other organizations etc.

            LinkEntity role = new LinkEntity();
            role.LinkFromEntityName = "role";
            role.LinkFromAttributeName = "roleid";
            role.LinkToEntityName = "systemuserroles";
            role.LinkToAttributeName = "roleid";

            LinkEntity userRoles = new LinkEntity();
            userRoles.LinkFromEntityName = "systemuserroles";
            userRoles.LinkFromAttributeName = "systemuserid";
            userRoles.LinkToEntityName = "systemuser";
            userRoles.LinkToAttributeName = "systemuserid";

            // WHERE
            ConditionExpression userCondition = new ConditionExpression("systemuserid", ConditionOperator.Equal, systemUserId);
            ConditionExpression roleCondition = new ConditionExpression("roleid", ConditionOperator.Equal, roleId);

            userRoles.LinkCriteria = new FilterExpression();
            userRoles.LinkCriteria.Conditions.Add(userCondition);

            role.LinkEntities.Add(userRoles);
            query.LinkEntities.Add(role);
            query.Criteria.AddCondition(roleCondition);

            EntityCollection roles = _service.RetrieveMultiple(query);

            return roles.Entities.Count > 0;
        }


        public static OptionMetadataCollection GetOptions(string entityName, string attributeName)
        {
            string optionsetText = string.Empty;
            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest();
            retrieveAttributeRequest.EntityLogicalName = entityName;
            retrieveAttributeRequest.LogicalName = attributeName;
            retrieveAttributeRequest.RetrieveAsIfPublished = true;

            RetrieveAttributeResponse retrieveAttributeResponse =
              (RetrieveAttributeResponse)_serviceProxy.Execute(retrieveAttributeRequest);
            try
            {
                PicklistAttributeMetadata picklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                OptionSetMetadata optionsetMetadata = picklistAttributeMetadata.OptionSet;
                return optionsetMetadata.Options;
            }
            catch (Exception)
            {
                BooleanAttributeMetadata booleanAttributeMetadata = (BooleanAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                return (new OptionMetadataCollection(new List<OptionMetadata>(new OptionMetadata[] { booleanAttributeMetadata.OptionSet.TrueOption, booleanAttributeMetadata.OptionSet.FalseOption })));
            }
        }

        public static void SetOptionValue(vrp_investdocument document, string fieldName, OptionSetValue value)
        {
            try
            {
                document.SetAttributeValue(fieldName, value);
                _serviceProxy.Update(document);
            }
            catch (Exception)
            {
                document.SetAttributeValue(fieldName, Convert.ToBoolean(value.Value));
                _serviceProxy.Update(document);
            }
            Thread.Sleep(1000);
        }

        public static void CreateActionRecord(vrp_investdocument document, vrp_actionconfig config)
        {
            Entity record = new Entity(vrp_action.EntityLogicalName);

            record.Attributes.Add("subject", config.vrp_description);
            record.Attributes.Add("vrp_actioncode", config.vrp_actioncode);
            record.Attributes.Add("regardingobjectid", document.Id);
            record.Attributes.Add("vrp_responsibleuser", new OptionSetValue(config.vrp_actionman.Value));

            _serviceProxy.Create(record);
        }

        public static vrp_investdocument GetDocument(string documentName)
        {
            try
            {
                QueryExpression getDocumentByName = new QueryExpression(vrp_investdocument.EntityLogicalName);
                getDocumentByName.ColumnSet = new ColumnSet(true);
                getDocumentByName.Criteria.AddCondition(new ConditionExpression("vrp_name", ConditionOperator.Like, "%" + documentName + "%"));
                EntityCollection ec = _serviceProxy.RetrieveMultiple(getDocumentByName);
                return ec.Entities.FirstOrDefault().ToEntity<vrp_investdocument>();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static vrp_action GetAction(string actionCode, vrp_investdocument document)
        {
            QueryExpression findAction = new QueryExpression(vrp_action.EntityLogicalName);
            findAction.ColumnSet = new ColumnSet(true);
            findAction.Criteria.AddCondition(new ConditionExpression("regardingobjectid", ConditionOperator.Equal, document.Id));
            findAction.Criteria.AddCondition(new ConditionExpression("vrp_actioncode", ConditionOperator.Equal, actionCode));
            try
            {
                return _serviceProxy.RetrieveMultiple(findAction).Entities.FirstOrDefault().ToEntity<vrp_action>();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static vrp_actionconfig GetActionConfig(string actionCode)
        {
            QueryExpression getConfigByActionCode = new QueryExpression(vrp_actionconfig.EntityLogicalName);
            getConfigByActionCode.ColumnSet = new ColumnSet(true);
            getConfigByActionCode.Criteria.AddCondition(new ConditionExpression("vrp_actioncode", ConditionOperator.Equal, actionCode));
            try
            {
                EntityCollection ec = _serviceProxy.RetrieveMultiple(getConfigByActionCode);
                return ec.Entities.FirstOrDefault().ToEntity<vrp_actionconfig>();
            }
            catch (Exception e)
            {
                Console.WriteLine("GetActionConfig is returning null due to an exception : " + e.Message);
                return null;
            }
        }

        public static int ReadSelection()
        {
            int decision = Convert.ToInt32(Console.ReadKey().KeyChar) - 48; Console.WriteLine(); return decision;
        }

        public static int CompareActionCodes(string code1, string code2)
        {
            int[] c1 = code1.Split('.').Select(n => Convert.ToInt32(n)).ToArray();
            int[] c2 = code2.Split('.').Select(n => Convert.ToInt32(n)).ToArray();

            if (c1.Length < c2.Length)
            {
                int[] tmp = new int[c2.Length];
                for (int i = 0; i < c1.Length; i++)
                    tmp[i] = c1[i];
                for (int i = c1.Length; i < c2.Length; i++)
                    tmp[i] = 0;
                c1 = tmp;
            }

            if (c2.Length < c1.Length)
            {
                int[] tmp = new int[c1.Length];
                for (int i = 0; i < c2.Length; i++)
                    tmp[i] = c2[i];
                for (int i = c2.Length; i < c1.Length; i++)
                    tmp[i] = 0;
                c2 = tmp;
            }

            for (int i = 0; i < c1.Length; i++)
            {
                if (c1[i] > c2[i])
                    return 1;
                else if (c1[i] < c2[i])
                    return -1;
                else
                    continue;
            }
            return 0;
        }

        public class ActionCodeComparer : IComparer<String>
        {
            public int Compare(string code1, string code2)
            {
                int[] c1 = code1.Split('.').Select(n => Convert.ToInt32(n)).ToArray();
                int[] c2 = code2.Split('.').Select(n => Convert.ToInt32(n)).ToArray();

                if (c1.Length < c2.Length)
                {
                    int[] tmp = new int[c2.Length];
                    for (int i = 0; i < c1.Length; i++)
                        tmp[i] = c1[i];
                    for (int i = c1.Length; i < c2.Length; i++)
                        tmp[i] = 0;
                    c1 = tmp;
                }

                if (c2.Length < c1.Length)
                {
                    int[] tmp = new int[c1.Length];
                    for (int i = 0; i < c2.Length; i++)
                        tmp[i] = c2[i];
                    for (int i = c2.Length; i < c1.Length; i++)
                        tmp[i] = 0;
                    c2 = tmp;
                }

                for (int i = 0; i < c1.Length; i++)
                {
                    if (c1[i] > c2[i])
                        return 1;
                    else if (c1[i] < c2[i])
                        return -1;
                    else
                        continue;
                }
                return 0;
            }
        }

        private class RoleComparer : IEqualityComparer<Role>
        {
            bool IEqualityComparer<Role>.Equals(Role x, Role y)
            {
                return x.Name.Equals(y.Name);
            }

            int IEqualityComparer<Role>.GetHashCode(Role obj)
            {
                return obj.Name.GetHashCode();
            }
        }

        public static void UndoAction(vrp_investdocument Document, string actionCode)
        {
            vrp_actionconfig config = GetActionConfig(actionCode);

            if (config != null)
            {
                // Get field lists
                List<string> fieldLists = new List<string>(new string[] {
                config.vrp_fieldinfo, 
                config.vrp_otherfields
                });
                if (config.Attributes.ContainsKey("vrp_preconditions"))
                    fieldLists.Add("vrp_preconditions");

                // Loop through field lists and set all fields to null.
                foreach (string fieldList in fieldLists)
                    foreach (string field in fieldList.Split('*'))
                        if (Document.Attributes.ContainsKey(field))
                            Document[field] = null;
                // Set rejected flag.
                Document.vrp_rejected = true;
                // Update document entity.
                //Vrp_InvestDocumentHandler.Update(Document);
                Vrp_ActionConfigWalker.Log.AppendLine(String.Format("{0} action undone.", actionCode));

                // Delete associated action record.
                vrp_action action = GetAction(actionCode, Document);
                if (action != null)
                {
                    //Vrp_ActionHandler.DeleteAction(action.Id);
                    Vrp_ActionConfigWalker.Log.AppendLine(String.Format("{0} action record deleted.", actionCode));
                }
                else
                {
                    Vrp_ActionConfigWalker.Log.AppendLine(String.Format("{0} action record was not found.", actionCode));
                }
            }
            else
                Vrp_ActionConfigWalker.Log.AppendLine(String.Format("{0} action config was null.", actionCode));
        }
    }
}