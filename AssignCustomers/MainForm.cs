﻿using AssignCustomers.Components;
using AssignCustomers.Components.Model;
using AssignCustomers.Components.Util;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssignCustomers
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            StartProgress("Loading configuration");

            this.configuration = new Configuration()
            {
                OrganizationUrl = Program.ReadSetting(Program.ConfigurationKeys.OrganizationUrl),
                Domain = Program.ReadSetting(Program.ConfigurationKeys.Domain),
                Username = Program.ReadSetting(Program.ConfigurationKeys.Username),
                Password = Program.ReadSetting(Program.ConfigurationKeys.Password),
                UseSSL = Program.ReadSetting(Program.ConfigurationKeys.UseSSL).Equals("True"),
                UseIFD = Program.ReadSetting(Program.ConfigurationKeys.UseIFD).Equals("True")
            };
            tbOrganizationUrl.Text = Configuration.OrganizationUrl;
            tbDomain.Text = Configuration.Domain;
            tbUsername.Text = Configuration.Username;
            tbPassword.Text = Configuration.Password;
            cbUseSSL.Checked = Configuration.UseSSL;
            cbUseIFD.Checked = Configuration.UseIFD;

            StopProgress();
        }

        private Configuration configuration = null;
        private List<Customer> results;

        public Configuration Configuration
        {
            get
            {
                return configuration ?? GetConfiguration();
            }
            set { configuration = value; }
        }

        public Configuration GetConfiguration()
        {
            return new Configuration()
            {
                OrganizationUrl = tbOrganizationUrl.Text,
                Domain = tbDomain.Text,
                Username = tbUsername.Text,
                Password = tbPassword.Text,
                UseSSL = cbUseSSL.Checked,
                UseIFD = cbUseIFD.Checked
            };
        }
        
        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            StartProgress("Testing connection.");
            BackgroundWorker worker = new BackgroundWorker();
            Connection connection = null;
            worker.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    this.configuration = new Configuration()
                    {
                        OrganizationUrl = tbOrganizationUrl.Text,
                        Domain = tbDomain.Text,
                        Username = tbUsername.Text,
                        Password = tbPassword.Text,
                        UseSSL = cbUseSSL.Checked,
                        UseIFD = cbUseIFD.Checked
                    };

                    connection = new Connection(configuration);
                });
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                delegate(object o, RunWorkerCompletedEventArgs args)
                {
                    StopProgress((connection != null && connection.IsAlive) ? "Connection successfully established." : "Connection failed. " + connection.LastError);
                    connection.Dispose();
                    connection = null;
                });
            worker.RunWorkerAsync();
        }

        private void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            StartProgress("Saving configuration...");
            try
            {
                this.configuration = new Configuration()
                {
                    OrganizationUrl = tbOrganizationUrl.Text,
                    Domain = tbDomain.Text,
                    Username = tbUsername.Text,
                    Password = tbPassword.Text,
                    UseSSL = cbUseSSL.Checked,
                    UseIFD = cbUseIFD.Checked
                };

                Program.UpdateSetting(Program.ConfigurationKeys.OrganizationUrl, Configuration.OrganizationUrl);
                Program.UpdateSetting(Program.ConfigurationKeys.Domain, Configuration.Domain);
                Program.UpdateSetting(Program.ConfigurationKeys.Username, Configuration.Username);
                Program.UpdateSetting(Program.ConfigurationKeys.Password, Configuration.Password);
                Program.UpdateSetting(Program.ConfigurationKeys.UseSSL, Configuration.UseSSL.ToString());
                Program.UpdateSetting(Program.ConfigurationKeys.UseIFD, Configuration.UseIFD.ToString());
                StopProgress("Successfully saved configuration.");
            }
            catch (Exception)
            {
                StopProgress("Couldn't save configuration.");
                return;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            txtPath.Enabled = true;
            openCsvFileDialog.Filter = "CSV files (*.csv)|*.csv";
            openCsvFileDialog.Multiselect = false;
            DialogResult result = openCsvFileDialog.ShowDialog(this);
            if (result.Equals(DialogResult.OK))
            {
                txtPath.Text = openCsvFileDialog.FileName;
                txtPath.Enabled = false;
                ParseFile(txtPath.Text);
            }
        }

        private void btnMatch_Click(object sender, EventArgs e)
        {
            StartProgress("Matching progress has begun.");

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += new DoWorkEventHandler(
            delegate(object o, DoWorkEventArgs args)
            {
                try
                {
                    results.ForEach(delegate(Customer c) { c.Collaborators = new Dictionary<string, List<Guid>>(); });
                    Connection conn = new Connection(GetConfiguration());
                    UpdateProgress("Matching owners..");
                    CrmLookupHelper.MatchOwners(results, conn.Service);
                    UpdateProgress("Matching accounts..");
                    CrmLookupHelper.MatchAccounts(results, conn.Service);
                    UpdateProgress("Matching collaborators..");
                    CrmLookupHelper.MatchCollaborators(results, conn.Service);
                    StopProgress("Matching completed.");
                }
                catch (Exception ex)
                {
                    StopProgress("Matching failed. " + ex.Message);
                    throw;
                }
            });

            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
            delegate(object o, RunWorkerCompletedEventArgs args)
            {
                List<Customer> nonMatched = (from c in results where c.AccountId.Equals(Guid.Empty) select c).ToList();
                List<Customer> ownerless = (from c in results where c.OwnerId.Equals(Guid.Empty) select c).ToList();
                List<Customer> errors = new List<Customer>(nonMatched);
                errors.AddRange(ownerless);

                UpdateProgress("Dumping errors...");
                DumpCustomers(nonMatched, "NonMatchedCustomers");
                DumpCustomers(ownerless, "OwnerlessCustomers");
                DumpCustomers(errors, "InvalidRows");

                int matchedAccounts = (from c in results where !c.AccountId.Equals(Guid.Empty) select c).Count();
                int validOwners = (from c in results where !c.OwnerId.Equals(Guid.Empty) select c).Count();
                int nonmatchedAccounts = nonMatched.Count;
                int invalidOwners = ownerless.Count;

                MessageBox.Show(
                    this,
                    String.Format("Match completed.\n - Matched {0} accounts.\n - {1} records had a valid owner.\n - {2} records weren't matched.\n - {3} records did not have a valid owner.", matchedAccounts, validOwners, nonmatchedAccounts, invalidOwners), "Match Results",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                UpdateProgress(String.Format("Match completed. Matched {0} accounts. {1} records had a valid owner. {2} records weren't matched. {3} records did not have a valid owner.", matchedAccounts, validOwners, nonmatchedAccounts, invalidOwners));
            });

            worker.RunWorkerAsync();

        }

        private void btnAssign_Click(object sender, EventArgs e)
        {
            StartProgress("Building requests...");

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += new DoWorkEventHandler(
            delegate(object o, DoWorkEventArgs args)
            {
                RequestBuilder builder = new RequestBuilder(results);
                builder.BuildAssignRequestCollection();
                builder.BuildGrantAccessRequestCollection();

                

                UpdateProgress("Executing assign requests");
                Execute(builder.AssignRequests);

                UpdateProgress("Executing grant access requests");
                Execute(builder.GrantAccessRequests);
            });

            worker.RunWorkerAsync();
        }

        private void ParseFile(String fileName)
        {
            BackgroundWorker worker = new BackgroundWorker();
            StartProgress("Parsing " + fileName);

            worker.DoWork += new DoWorkEventHandler(
            delegate(object o, DoWorkEventArgs args)
            {
                try
                {
                    CustomerParser parser = new CustomerParser(fileName);
                    parser.HasHeaders = cbHasHeaders.Checked;
                    results = parser.Parse();
                    StopProgress("Successfully parsed the CSV file.");
                    SetText(lblCount, String.Format("Record Count : {0}", results.Count));
                }
                catch (Exception e)
                {
                    StopProgress("Could not parse the CSV file. " + e.Message);
                    SetText(lblCount, String.Format("Record Count : {0}", "N/A"));
                }
            });
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
            delegate(object o, RunWorkerCompletedEventArgs args)
            {
                SetEnabled(btnMatch, true);
            });
            worker.RunWorkerAsync();
        }

        private void Execute(OrganizationRequestCollection collection)
        {
            StartProgress("Starting to execute request collection.");
            Connection connection = new Connection(GetConfiguration());
            int lapSize = (int)numLapSize.Value;
            double laps = Math.Ceiling((double)collection.Count / (double)lapSize);
            int lap = 0;
            int errors = 0;
            int successes = 0;
            int ctr = 0;
            while (lap < laps)
            {
                OrganizationRequestCollection tmp = new OrganizationRequestCollection();
                tmp.AddRange(collection.Skip(lap * lapSize).Take(lapSize));

                foreach (OrganizationRequest request in tmp)
                {
                    ctr++;
                    UpdateProgress(String.Format("Executing request {2} of {3} in {0} / {1} laps.", lap + 1, (int)laps, ctr, lapSize));
                    OrganizationResponse response = (OrganizationResponse)connection.Service.Execute(request);
                    if (response.Results != null)
                        successes++;
                    else
                        errors++; 
                }
                ctr = 0;
                lap++;
            }
            StopProgress("Finished executing requests.");
        }

        private void ExecuteMultiple(OrganizationRequestCollection collection)
        {
            StartProgress("Starting to execute request collection.");
            Connection connection = new Connection(Configuration);
            int lapSize = (int)numLapSize.Value;
            double laps = Math.Ceiling((double)collection.Count / (double)lapSize);
            int lap = 0;
            int errors = 0;
            int successes = 0;
            while (lap < laps)
            {
                UpdateProgress(String.Format("Executing {0} / {1} laps.", lap + 1, (int)laps));
                OrganizationRequestCollection tmp = new OrganizationRequestCollection();
                tmp.AddRange(collection.Skip(lap * lapSize).Take(lapSize));
                ExecuteMultipleRequest request = new ExecuteMultipleRequest()
                {
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    },
                    Requests = tmp
                };

                ExecuteMultipleResponse response = (ExecuteMultipleResponse)connection.Service.Execute(request);
                errors += response.Responses.Where(r => r.Fault != null).Count();
                successes += response.Responses.Where(r => r.Response != null).Count();

                lap++;
            }
            StopProgress("Finished executing requests.");
        }

        private void DumpCustomers(IEnumerable<Customer> customers, string filename = "CustomersDump")
        {
            try
            {

                StringBuilder dmp = new StringBuilder();
                dmp.AppendFormat("AccountNumber, OwnerEMail, Collabs").AppendLine();
                foreach (Customer customer in customers)
                {
                    dmp.AppendFormat("{0};{1};{2}", customer.CustomerNumber, customer.OwnerEmailAddress, String.Join(",", customer.CollaboratorsEmailAddresses)).AppendLine();
                }
                File.WriteAllText(String.Format("{0}.dmp", filename), dmp.ToString());
            }
            catch (Exception)
            {
                UpdateProgress("An error occured during the dump process.");
            }
        }
        
        protected void SetText(Control ctl, string message)
        {
            this.Invoke((Action)delegate
            {
                ctl.Text = message;
            });
        }
       
        protected void SetEnabled(Control ctl, bool enabled)
        {
            this.Invoke((Action)delegate
            {
                ctl.Enabled = enabled;
            });
        }

        private void StartProgress()
        {
            this.Invoke((Action)delegate
            {
                lblStatus.Text = "Working ...";
                tsProgress.Style = ProgressBarStyle.Marquee;
            });
        }

        private void StartProgress(string message)
        {
            this.Invoke((Action)delegate
            {
                lblStatus.Text = message;
                tsProgress.Style = ProgressBarStyle.Marquee;
            });
        }

        private void UpdateProgress(string message)
        {
            this.Invoke((Action)delegate
            {
                lblStatus.Text = message;
            });
        }

        private void StopProgress()
        {
            this.Invoke((Action)delegate
            {
                lblStatus.Text = "Idle.";
                tsProgress.Style = ProgressBarStyle.Continuous;
            });
        }

        private void StopProgress(string message)
        {
            this.Invoke((Action)delegate
            {
                lblStatus.Text = message;
                tsProgress.Style = ProgressBarStyle.Continuous;
            });
        }

        private void cbHasHeaders_CheckedChanged(object sender, EventArgs e)
        {
            txtPath.Text = string.Empty;
            txtPath.Enabled = true;
            lblCount.Text = "Record Count : N/A";
        }
    }
}
