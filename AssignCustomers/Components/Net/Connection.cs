﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignCustomers.Components
{
    class Connection : IDisposable
    {
        public Configuration Configuration { get; set; }
        public string LastError { get; set; }
        public Guid ContextUserId { get; set; }
        public bool IsAlive 
        {
            get
            {
                try
                {
                    ContextUserId = ((WhoAmIResponse)service.Execute(new WhoAmIRequest())).UserId;
                    return !ContextUserId.Equals(Guid.Empty);
                }
                catch (Exception e)
                {
                    // TODO: Handle / Log the exception.
                    this.LastError = e.Message;
                    return false;
                }
            }
        }

        private IOrganizationService service;
        public IOrganizationService Service 
        {
            get { return service; } 
        }

        public Connection(Configuration configuration)
        {
            this.Configuration = configuration;
            Connect();
        }

        private void Connect()
        {
            try
            {
                string connectionString = "";
                connectionString = String.Format(
                    "Url=http{0}://{1}; Username={2}\\{3}; Password={4};",
                    Configuration.UseSSL ? "s" : "",
                    Configuration.OrganizationUrl,
                    Configuration.Domain,
                    Configuration.Username,
                    Configuration.Password
                );

                this.service = new OrganizationService(CrmConnection.Parse(connectionString));

                ContextUserId = ((WhoAmIResponse)service.Execute(new WhoAmIRequest())).UserId;
            }
            catch (Exception e)
            {
                // TODO: Handle / Log the exception.
                this.LastError = e.Message;
                throw;
            }
        }

        public void Dispose()
        {
            service = null;
            Configuration = null;
        }
    }
}
