﻿using AssignCustomers.Components.Model;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignCustomers.Components.Util
{
    class RequestBuilder
    {
        List<Customer> customers;
        OrganizationRequestCollection assignRequests;
        Connection connection;

        public OrganizationRequestCollection AssignRequests
        {
            get { return assignRequests; }
        }
        OrganizationRequestCollection grantAccessRequests;

        public OrganizationRequestCollection GrantAccessRequests
        {
            get { return grantAccessRequests; }
        }

        public RequestBuilder(List<Customer> cList) 
        { 
            this.customers = cList; 
        }

        public OrganizationRequestCollection BuildAssignRequestCollection()
        {
            assignRequests = new OrganizationRequestCollection();
            List<Customer> tmp = new List<Customer>(customers);
            tmp.RemoveAll(c => c.AccountId.Equals(Guid.Empty) || c.OwnerId.Equals(Guid.Empty));
            foreach (Customer customer in tmp) 
            {
                assignRequests.Add(new AssignRequest()
                {
                    Assignee = new EntityReference("systemuser", customer.OwnerId),
                    Target = new EntityReference("account", customer.AccountId)
                });
            }
            return assignRequests;
        }

        public OrganizationRequestCollection BuildGrantAccessRequestCollection()
        {
            grantAccessRequests = new OrganizationRequestCollection();
            List<Customer> tmp = new List<Customer>(customers);
            tmp.RemoveAll(c => c.AccountId.Equals(Guid.Empty));
            foreach (Customer customer in tmp)
            {
                foreach (var item in customer.Collaborators)
                {
                    foreach (Guid id in item.Value)
                    {
                        if (!id.Equals(Guid.Empty))
                        {
                            grantAccessRequests.Add(new GrantAccessRequest()
                            {
                                PrincipalAccess = new PrincipalAccess()
                                {
                                    Principal = new EntityReference(item.Key, id),
                                    AccessMask = AccessRights.AppendAccess | AccessRights.AppendToAccess | AccessRights.ReadAccess | AccessRights.WriteAccess
                                },
                                Target = new EntityReference("account", customer.AccountId)
                            });
                        }
                    }
                }
            }

            return grantAccessRequests;
        }

    }
}
