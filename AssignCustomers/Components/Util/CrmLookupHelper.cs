﻿using AssignCustomers.Components.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignCustomers.Components.Util
{
    static class CrmLookupHelper
    {
        public static void MatchAccounts(List<Customer> customers, IOrganizationService service)
        {
            int page = 0;
            int fetchCount = 2000;
            while (page < Math.Ceiling((double)customers.Count / (double)fetchCount))
            {
                string[] customerNumbers = (from c in customers select c.CustomerNumber).Distinct().Skip((page) * fetchCount).Take(fetchCount).ToArray();
                QueryExpression retrieveAccounts = new QueryExpression("account");
                retrieveAccounts.ColumnSet = new ColumnSet("accountid", "accountnumber");
                retrieveAccounts.Criteria.AddCondition("accountnumber", ConditionOperator.In, customerNumbers);
                EntityCollection collection = service.RetrieveMultiple(retrieveAccounts);
                
                foreach (Customer customer in customers)
                {
                    var accountId = (from e in collection.Entities
                                     where e["accountnumber"].Equals(customer.CustomerNumber)
                                     select e["accountid"]).FirstOrDefault();
                    if(accountId != null)
                        customer.AccountId = new Guid(accountId.ToString());
                }
                page++;
            }
        }

        public static void MatchOwners(List<Customer> customers, IOrganizationService service)
        {
            string[] ownerEmailAddresses = (from c in customers select c.OwnerEmailAddress).Distinct().ToArray();
            QueryExpression retrieveSystemUsers = new QueryExpression("systemuser");
            retrieveSystemUsers.ColumnSet = new ColumnSet("systemuserid", "internalemailaddress");
            retrieveSystemUsers.Criteria.AddCondition("internalemailaddress", ConditionOperator.In, ownerEmailAddresses);
            EntityCollection collection = service.RetrieveMultiple(retrieveSystemUsers);

            foreach (Customer customer in customers)
            {
                var ownerId = (from e in collection.Entities
                               where e["internalemailaddress"].ToString().ToLowerInvariant().Equals(customer.OwnerEmailAddress)
                               select e["systemuserid"]).FirstOrDefault();
                customer.OwnerId = (ownerId == null) ? Guid.Empty : new Guid(ownerId.ToString());
            }
        }

        public static void MatchCollaborators(List<Customer> customers, IOrganizationService service)
        {
            #region İğrenç
		string[][] mails = (from c in customers select c.CollaboratorsEmailAddresses.Where(cc => cc.Contains("@")).ToArray()).ToArray();
            List<string> collaboratorEmailAddresses = new List<string>();
            for (int i = 0; i < mails.Length; i++)
                for (int j = 0; j < mails[i].Length; j++)
                    if (!collaboratorEmailAddresses.Contains(mails[i][j]))
                        collaboratorEmailAddresses.Add(mails[i][j]);

            List<string> collaboratorTeamNames = new List<string>();
            string[][] nonMails = (from c in customers select c.CollaboratorsEmailAddresses.Where(cc => !cc.Contains("@")).ToArray()).ToArray();
            for (int i = 0; i < nonMails.Length; i++)
                for (int j = 0; j < nonMails[i].Length; j++)
                    if (!collaboratorTeamNames.Contains(nonMails[i][j]))
                        collaboratorTeamNames.Add(nonMails[i][j]);
            #endregion

            QueryExpression retrieveSystemUsers = new QueryExpression("systemuser");
            retrieveSystemUsers.ColumnSet = new ColumnSet("systemuserid", "internalemailaddress");
            retrieveSystemUsers.Criteria.AddCondition("internalemailaddress", ConditionOperator.In, collaboratorEmailAddresses.ToArray());
            EntityCollection userCollection = service.RetrieveMultiple(retrieveSystemUsers);
            
            QueryExpression retrieveTeams = new QueryExpression("team");
            retrieveTeams.ColumnSet = new ColumnSet("teamid", "name");
            retrieveTeams.Criteria.AddCondition("name", ConditionOperator.In, collaboratorEmailAddresses.ToArray());
            EntityCollection teamCollection = service.RetrieveMultiple(retrieveTeams);

            foreach (Customer customer in customers)
            {
                List<Guid> collabId = (from e in userCollection.Entities
                                       where customer.CollaboratorsEmailAddresses.Contains(e["internalemailaddress"].ToString().ToLowerInvariant())
                               select new Guid(e["systemuserid"].ToString())).ToList();
                if (!customer.Collaborators.ContainsKey("systemuser"))
                    customer.Collaborators.Add("systemuser", collabId);
                else
                    customer.Collaborators["systemuser"].AddRange(collabId);
            }

            foreach (Customer customer in customers)
            {
                List<Guid> teamId = (from e in teamCollection.Entities
                                     where customer.CollaboratorsEmailAddresses.Contains(e["name"].ToString().ToLowerInvariant())
                               select new Guid(e["teamid"].ToString())).ToList();
                if (!customer.Collaborators.ContainsKey("team"))
                    customer.Collaborators.Add("team", teamId);
                else
                    customer.Collaborators["team"].AddRange(teamId);
            }
        }
    }
}
