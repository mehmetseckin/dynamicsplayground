﻿using AssignCustomers.Components.Model;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignCustomers.Components.Util
{
    class CustomerParser
    {
        private string fileName;
        public bool HasHeaders { get; set; }
        private List<Customer> results;
        public List<Customer> Results
        {
            get { return results; }
            set { results = value; }
        }

        private string delimiters;

        public CustomerParser(String fileName)
        {
            this.fileName = fileName;
            results = new List<Customer>();
            delimiters = Program.ReadSetting(Program.ConfigurationKeys.Delimiters);
            if (delimiters.Equals(string.Empty)) delimiters = ";";
            HasHeaders = false;
        }
        public CustomerParser(String fileName, bool hasHeaders)
        {
            this.fileName = fileName;
            results = new List<Customer>();
            delimiters = Program.ReadSetting(Program.ConfigurationKeys.Delimiters);
            if (delimiters.Equals(string.Empty)) delimiters = ";";
            this.HasHeaders = hasHeaders;
        }

        public List<Customer> Parse()
        {
            bool passedHeader = false;
            using (TextFieldParser parser = new TextFieldParser(fileName))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiters);

                while (!parser.EndOfData)
                {
                    if (HasHeaders && !passedHeader) { parser.ReadFields(); passedHeader = true; continue; }
                    string[] fields = parser.ReadFields();
               
                    results.Add(new Customer()
                    {
                        CustomerNumber = fields[0],
                        OwnerEmailAddress = fields[1].ToLowerInvariant(),
                        CollaboratorsEmailAddresses = fields[2].ToLowerInvariant().Split(',').ToList<string>(),
                        Collaborators = new Dictionary<string,List<Guid>>()
                    });
                }

            }

            return results;
        }
    }
}
