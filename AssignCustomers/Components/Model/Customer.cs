﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignCustomers.Components.Model
{
    class Customer
    {
        public string CustomerNumber { get; set; }
        public string OwnerEmailAddress { get; set; }
        public List<string> CollaboratorsEmailAddresses { get; set; }
        public Guid AccountId { get; set; }
        public Guid OwnerId { get; set; }
        public Dictionary<string, List<Guid>> Collaborators { get; set; }
    }
}
