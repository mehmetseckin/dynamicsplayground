﻿namespace AssignCustomers
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tbDomain = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.MaskedTextBox();
            this.tbOrganizationUrl = new System.Windows.Forms.TextBox();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.ConnectionSettingsTab = new System.Windows.Forms.TabPage();
            this.btnSaveConfiguration = new System.Windows.Forms.Button();
            this.gbOtherSettings = new System.Windows.Forms.GroupBox();
            this.cbUseIFD = new System.Windows.Forms.CheckBox();
            this.cbUseSSL = new System.Windows.Forms.CheckBox();
            this.gbCredentials = new System.Windows.Forms.GroupBox();
            this.lblOrganizationUrl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.CustomersTab = new System.Windows.Forms.TabPage();
            this.gbSelectCSVFile = new System.Windows.Forms.GroupBox();
            this.btnMatch = new System.Windows.Forms.Button();
            this.lblCount = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.openCsvFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnAssign = new System.Windows.Forms.Button();
            this.cbHasHeaders = new System.Windows.Forms.CheckBox();
            this.numLapSize = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tcMain.SuspendLayout();
            this.ConnectionSettingsTab.SuspendLayout();
            this.gbOtherSettings.SuspendLayout();
            this.gbCredentials.SuspendLayout();
            this.CustomersTab.SuspendLayout();
            this.gbSelectCSVFile.SuspendLayout();
            this.StatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLapSize)).BeginInit();
            this.SuspendLayout();
            // 
            // tbDomain
            // 
            this.tbDomain.Location = new System.Drawing.Point(106, 48);
            this.tbDomain.Name = "tbDomain";
            this.tbDomain.Size = new System.Drawing.Size(141, 20);
            this.tbDomain.TabIndex = 2;
            this.tbDomain.Tag = "";
            this.tbDomain.Text = "MYDOMAIN";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(106, 100);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '⚫';
            this.tbPassword.Size = new System.Drawing.Size(141, 20);
            this.tbPassword.TabIndex = 4;
            this.tbPassword.Text = "password";
            // 
            // tbOrganizationUrl
            // 
            this.tbOrganizationUrl.Location = new System.Drawing.Point(106, 22);
            this.tbOrganizationUrl.Name = "tbOrganizationUrl";
            this.tbOrganizationUrl.Size = new System.Drawing.Size(192, 20);
            this.tbOrganizationUrl.TabIndex = 1;
            this.tbOrganizationUrl.Text = "orgname.mycompany.com";
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(106, 74);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(141, 20);
            this.tbUsername.TabIndex = 3;
            this.tbUsername.Text = "bill.gates";
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.ConnectionSettingsTab);
            this.tcMain.Controls.Add(this.CustomersTab);
            this.tcMain.Location = new System.Drawing.Point(12, 12);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(502, 215);
            this.tcMain.TabIndex = 8;
            // 
            // ConnectionSettingsTab
            // 
            this.ConnectionSettingsTab.Controls.Add(this.btnSaveConfiguration);
            this.ConnectionSettingsTab.Controls.Add(this.gbOtherSettings);
            this.ConnectionSettingsTab.Controls.Add(this.gbCredentials);
            this.ConnectionSettingsTab.Controls.Add(this.btnTestConnection);
            this.ConnectionSettingsTab.Location = new System.Drawing.Point(4, 22);
            this.ConnectionSettingsTab.Name = "ConnectionSettingsTab";
            this.ConnectionSettingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.ConnectionSettingsTab.Size = new System.Drawing.Size(494, 189);
            this.ConnectionSettingsTab.TabIndex = 1;
            this.ConnectionSettingsTab.Text = "Connection Settings";
            this.ConnectionSettingsTab.UseVisualStyleBackColor = true;
            // 
            // btnSaveConfiguration
            // 
            this.btnSaveConfiguration.Location = new System.Drawing.Point(266, 159);
            this.btnSaveConfiguration.Name = "btnSaveConfiguration";
            this.btnSaveConfiguration.Size = new System.Drawing.Size(108, 23);
            this.btnSaveConfiguration.TabIndex = 11;
            this.btnSaveConfiguration.Text = "Save Configuration";
            this.btnSaveConfiguration.UseVisualStyleBackColor = true;
            this.btnSaveConfiguration.Click += new System.EventHandler(this.btnSaveConfiguration_Click);
            // 
            // gbOtherSettings
            // 
            this.gbOtherSettings.Controls.Add(this.cbUseIFD);
            this.gbOtherSettings.Controls.Add(this.cbUseSSL);
            this.gbOtherSettings.Location = new System.Drawing.Point(318, 6);
            this.gbOtherSettings.Name = "gbOtherSettings";
            this.gbOtherSettings.Size = new System.Drawing.Size(170, 147);
            this.gbOtherSettings.TabIndex = 10;
            this.gbOtherSettings.TabStop = false;
            this.gbOtherSettings.Text = "Other Settings";
            // 
            // cbUseIFD
            // 
            this.cbUseIFD.AutoSize = true;
            this.cbUseIFD.Location = new System.Drawing.Point(7, 48);
            this.cbUseIFD.Name = "cbUseIFD";
            this.cbUseIFD.Size = new System.Drawing.Size(65, 17);
            this.cbUseIFD.TabIndex = 6;
            this.cbUseIFD.Text = "Use IFD";
            this.cbUseIFD.UseVisualStyleBackColor = true;
            // 
            // cbUseSSL
            // 
            this.cbUseSSL.AutoSize = true;
            this.cbUseSSL.Location = new System.Drawing.Point(7, 24);
            this.cbUseSSL.Name = "cbUseSSL";
            this.cbUseSSL.Size = new System.Drawing.Size(68, 17);
            this.cbUseSSL.TabIndex = 5;
            this.cbUseSSL.Text = "Use SSL";
            this.cbUseSSL.UseVisualStyleBackColor = true;
            // 
            // gbCredentials
            // 
            this.gbCredentials.Controls.Add(this.lblOrganizationUrl);
            this.gbCredentials.Controls.Add(this.tbPassword);
            this.gbCredentials.Controls.Add(this.label4);
            this.gbCredentials.Controls.Add(this.tbDomain);
            this.gbCredentials.Controls.Add(this.label3);
            this.gbCredentials.Controls.Add(this.tbUsername);
            this.gbCredentials.Controls.Add(this.label2);
            this.gbCredentials.Controls.Add(this.tbOrganizationUrl);
            this.gbCredentials.Location = new System.Drawing.Point(6, 6);
            this.gbCredentials.Name = "gbCredentials";
            this.gbCredentials.Size = new System.Drawing.Size(306, 147);
            this.gbCredentials.TabIndex = 9;
            this.gbCredentials.TabStop = false;
            this.gbCredentials.Text = "Credentials";
            // 
            // lblOrganizationUrl
            // 
            this.lblOrganizationUrl.AutoSize = true;
            this.lblOrganizationUrl.Location = new System.Drawing.Point(6, 25);
            this.lblOrganizationUrl.Name = "lblOrganizationUrl";
            this.lblOrganizationUrl.Size = new System.Drawing.Size(94, 13);
            this.lblOrganizationUrl.TabIndex = 4;
            this.lblOrganizationUrl.Text = "Organization URL ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Domain Name";
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Location = new System.Drawing.Point(380, 159);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(108, 23);
            this.btnTestConnection.TabIndex = 7;
            this.btnTestConnection.Text = "Test Connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // CustomersTab
            // 
            this.CustomersTab.Controls.Add(this.label1);
            this.CustomersTab.Controls.Add(this.numLapSize);
            this.CustomersTab.Controls.Add(this.btnAssign);
            this.CustomersTab.Controls.Add(this.gbSelectCSVFile);
            this.CustomersTab.Location = new System.Drawing.Point(4, 22);
            this.CustomersTab.Name = "CustomersTab";
            this.CustomersTab.Padding = new System.Windows.Forms.Padding(3);
            this.CustomersTab.Size = new System.Drawing.Size(494, 189);
            this.CustomersTab.TabIndex = 2;
            this.CustomersTab.Text = "Assign Customers";
            this.CustomersTab.UseVisualStyleBackColor = true;
            // 
            // gbSelectCSVFile
            // 
            this.gbSelectCSVFile.Controls.Add(this.cbHasHeaders);
            this.gbSelectCSVFile.Controls.Add(this.btnMatch);
            this.gbSelectCSVFile.Controls.Add(this.lblCount);
            this.gbSelectCSVFile.Controls.Add(this.btnBrowse);
            this.gbSelectCSVFile.Controls.Add(this.txtPath);
            this.gbSelectCSVFile.Location = new System.Drawing.Point(7, 4);
            this.gbSelectCSVFile.Name = "gbSelectCSVFile";
            this.gbSelectCSVFile.Size = new System.Drawing.Size(481, 100);
            this.gbSelectCSVFile.TabIndex = 0;
            this.gbSelectCSVFile.TabStop = false;
            this.gbSelectCSVFile.Text = "Select CSV File";
            // 
            // btnMatch
            // 
            this.btnMatch.Enabled = false;
            this.btnMatch.Location = new System.Drawing.Point(7, 71);
            this.btnMatch.Name = "btnMatch";
            this.btnMatch.Size = new System.Drawing.Size(75, 23);
            this.btnMatch.TabIndex = 3;
            this.btnMatch.Text = "Match";
            this.btnMatch.UseVisualStyleBackColor = true;
            this.btnMatch.Click += new System.EventHandler(this.btnMatch_Click);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(7, 48);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(102, 13);
            this.lblCount.TabIndex = 2;
            this.lblCount.Text = "Record Count : N/A";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(400, 19);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(7, 21);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(387, 20);
            this.txtPath.TabIndex = 0;
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.toolStripStatusLabel1,
            this.tsProgress});
            this.StatusBar.Location = new System.Drawing.Point(0, 238);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(522, 22);
            this.StatusBar.TabIndex = 6;
            this.StatusBar.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(26, 17);
            this.lblStatus.Text = "Idle";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(379, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // tsProgress
            // 
            this.tsProgress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsProgress.Name = "tsProgress";
            this.tsProgress.Size = new System.Drawing.Size(100, 16);
            this.tsProgress.Step = 0;
            // 
            // openCsvFileDialog
            // 
            this.openCsvFileDialog.FileName = "*.csv";
            this.openCsvFileDialog.Title = "Select CSV File";
            // 
            // btnAssign
            // 
            this.btnAssign.Location = new System.Drawing.Point(14, 111);
            this.btnAssign.Name = "btnAssign";
            this.btnAssign.Size = new System.Drawing.Size(75, 23);
            this.btnAssign.TabIndex = 1;
            this.btnAssign.Text = "Start";
            this.btnAssign.UseVisualStyleBackColor = true;
            this.btnAssign.Click += new System.EventHandler(this.btnAssign_Click);
            // 
            // cbHasHeaders
            // 
            this.cbHasHeaders.AutoSize = true;
            this.cbHasHeaders.Location = new System.Drawing.Point(387, 75);
            this.cbHasHeaders.Name = "cbHasHeaders";
            this.cbHasHeaders.Size = new System.Drawing.Size(88, 17);
            this.cbHasHeaders.TabIndex = 4;
            this.cbHasHeaders.Text = "Has Headers";
            this.cbHasHeaders.UseVisualStyleBackColor = true;
            this.cbHasHeaders.CheckedChanged += new System.EventHandler(this.cbHasHeaders_CheckedChanged);
            // 
            // numLapSize
            // 
            this.numLapSize.Location = new System.Drawing.Point(71, 147);
            this.numLapSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLapSize.Name = "numLapSize";
            this.numLapSize.Size = new System.Drawing.Size(80, 20);
            this.numLapSize.TabIndex = 2;
            this.numLapSize.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Lap Size :";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 260);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.tcMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "POAS CRM Customer Assign Tool";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tcMain.ResumeLayout(false);
            this.ConnectionSettingsTab.ResumeLayout(false);
            this.gbOtherSettings.ResumeLayout(false);
            this.gbOtherSettings.PerformLayout();
            this.gbCredentials.ResumeLayout(false);
            this.gbCredentials.PerformLayout();
            this.CustomersTab.ResumeLayout(false);
            this.CustomersTab.PerformLayout();
            this.gbSelectCSVFile.ResumeLayout(false);
            this.gbSelectCSVFile.PerformLayout();
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLapSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbDomain;
        private System.Windows.Forms.MaskedTextBox tbPassword;
        private System.Windows.Forms.TextBox tbOrganizationUrl;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage ConnectionSettingsTab;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblOrganizationUrl;
        private System.Windows.Forms.GroupBox gbOtherSettings;
        private System.Windows.Forms.CheckBox cbUseIFD;
        private System.Windows.Forms.CheckBox cbUseSSL;
        private System.Windows.Forms.GroupBox gbCredentials;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripProgressBar tsProgress;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnSaveConfiguration;
        private System.Windows.Forms.TabPage CustomersTab;
        private System.Windows.Forms.OpenFileDialog openCsvFileDialog;
        private System.Windows.Forms.GroupBox gbSelectCSVFile;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Button btnMatch;
        private System.Windows.Forms.Button btnAssign;
        private System.Windows.Forms.CheckBox cbHasHeaders;
        private System.Windows.Forms.NumericUpDown numLapSize;
        private System.Windows.Forms.Label label1;
    }
}

